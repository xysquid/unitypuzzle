﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scalable_camera : MonoBehaviour {

	private int screen_width = 0;
	private int screen_height = 0;

	// using this script to scale different scences
	// InGame (block grid), levels and worlds
	public float target_width_portrait;
	public float target_height_portrait;

	public float target_width_landscape;
	public float target_height_landscape;

	public Camera my_camera;

	// Use this for initialization
	void Start () {
		screen_width = Screen.width;
		screen_height = Screen.height;

		my_camera = gameObject.GetComponent<Camera>();

		on_resize ();
	
	}

	public void on_resize () {
		float TARGET_WIDTH = target_width_portrait;//720.0f;
		float TARGET_HEIGHT = target_height_portrait;//720.0f;

		if ((float)Screen.width > (float)Screen.height) {
			TARGET_WIDTH = target_width_landscape;//720.0f;
			TARGET_HEIGHT = target_height_landscape;//720.0f;
		}

		int PIXELS_TO_UNITS = 60; // 1:1 ratio of pixels to units

		float desiredRatio = TARGET_WIDTH / TARGET_HEIGHT;
		float currentRatio = (float)Screen.width/(float)Screen.height;

		if(currentRatio >= desiredRatio)
		{
			
			// Our resolution has plenty of width, so we just need to use the height to determine the camera size
			//Camera.main.orthographicSize = TARGET_HEIGHT / 4 / PIXELS_TO_UNITS;
			my_camera.orthographicSize = TARGET_HEIGHT / 4 / PIXELS_TO_UNITS;
		}
		else
		{
			// Our camera needs to zoom out further than just fitting in the height of the image.
			// Determine how much bigger it needs to be, then apply that to our original algorithm.
			float differenceInSize = desiredRatio / currentRatio;
			//Camera.main.orthographicSize = TARGET_HEIGHT / 4 / PIXELS_TO_UNITS * differenceInSize;
			my_camera.orthographicSize = TARGET_HEIGHT / 4 / PIXELS_TO_UNITS * differenceInSize;
		}

		//float headspace = 

		//float upper_target_y = 5 * PIXELS_TO_UNITS;

		//float target_

		//float headspace = screen_height*0.01f*0.5f - grid_h*0.5f*PIXELS_TO_UNITS;
	}
	
	// Update is called once per frame
	void Update () {
		if (screen_width != Screen.width ||
		    screen_height != Screen.height) {

				screen_width = Screen.width;
				screen_height = Screen.height;
				on_resize ();
		}
	}
}
