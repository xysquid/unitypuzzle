﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class level_grid : MonoBehaviour {

	public GameObject level_prefab;

	private GameObject[,] levels = new GameObject[5, 6];
	static int starting_level = 0;

	static int max_levels = 168;

	public Sprite eye_sprite;
	public Sprite hand_sprite;
	public Sprite eight_sprite;
	public Sprite heart_sprite;
	public Sprite crown_sprite;
	public Sprite eyerepeat_sprite;
	public Sprite zap_sprite;
	public Sprite eyebracket_sprite;
	public Sprite totalmines_sprite;
	public Sprite totaladjacentmines_sprite;
	public Sprite compass_sprite;
	public Sprite jointut_sprite;
	public Sprite sharetut_sprite;

	private static bool touchable_buttons = true;
	//private float resting_x = transform.position.x;

	public static level_grid level_grid_instance;

	// Use this for initialization
	void Start () {

		// touchable_buttons = false;
		//transform.position = new Vector3 (500f, transform.position.y, transform.position.z);
		if (level_grid_instance == null) {
			level_grid_instance = this;
			DontDestroyOnLoad (transform.root.gameObject);
			level_grid.level_grid_instance.setup ();
			level_grid.level_grid_instance.load_page ();
		} else {
			DestroyObject(gameObject);
		}


	}

	private void setup() {
		for (int x = 0; x < 5; x++) {
			for (int y = 0; y < 6; y++) {
				levels [x, y] = Instantiate (level_prefab, new Vector2 (x * 1.5f - 5.5f, 7 - y * 1f - 3.5f), Quaternion.identity) as GameObject;
				//GameObject new_block = Instantiate (block_prefab, new Vector2 (x * tile_size, y * tile_size), Quaternion.identity) as GameObject;
				//new_block.transform.position = (new Vector2 (x * tile_size, y * tile_size));
				//new_block.block_script.game_state = transform;
				//blocks[x][y] = new_block;
				levels[x,y].transform.SetParent (transform, false);

				level_btn_script l_script = levels [x, y].GetComponent<level_btn_script>();
				l_script.theparent = this;

				//l_script.textobj.guiText = l_script.levelnum;
				///b_script.y = y;
				//b_script.set_type (2);
				//b_script.game_state = gameObject;

				//blocks [x, y].transform.GetComponent<block_script>.y = y;
				//blocks [x, y].GetComponent<block_script>.set_type (1);

			}
		}
	}

	public void load_page() {
		transform.position = new Vector3 (0, 0, 0);
		for (int x = 0; x < 5; x++) {
			for (int y = 0; y < 6; y++) {
				level_btn_script l_script = levels [x, y].GetComponent<level_btn_script>();
				l_script.levelnum = x + y*5 + starting_level;

				l_script.show_text ();
				if (scene_manager.scene_manager_instance.current_world == 0) l_script.add_icon ();

				l_script.set_done_or_not (false);

				if (scene_manager.scene_manager_instance.current_world == 0) {
					if (PlayerPrefs.GetInt ("mainlevel" + l_script.levelnum, 0) == 1) {
						l_script.set_done_or_not (true);
					}
				} else {
					int progress_ = PlayerPrefs.GetInt ("worldprogress" + scene_manager.scene_manager_instance.current_world, 0);
					if (progress_ > l_script.levelnum) {
						l_script.set_done_or_not (true);
					} else if (progress_ < l_script.levelnum) {
						//l_script.set_locked ();
					}
				}

				if (l_script.levelnum > max_levels)
					l_script.hide ();
				else
					l_script.make_vis ();
			}
		}
	
	}


	IEnumerator page_right () {
		for (float x = transform.position.x; x > -500f; x -= 0.01f + 0.005f*x*x) {
			transform.position = new Vector3 ((float)x, transform.position.y, transform.position.z);

			yield return null;
		}

		//transform.position = new Vector3 (+1000, 0, 0);
		scene_manager.scene_manager_instance.goto_overworld ();
	}

	public void next_page() {
		if (level_grid.starting_level > level_grid.max_levels - 30)
			return;

		level_grid.starting_level += 30;

		level_grid.touchable_buttons = false;	// ignore any input while doing the swoop out effect

		//StartCoroutine("page_right");

		scene_manager.scene_manager_instance.goto_overworld ();

	}

	public void prev_page() {
		if (level_grid.starting_level <= 29)
			return;
		level_grid.starting_level -= 30;

		level_grid.touchable_buttons = false;

		scene_manager.scene_manager_instance.goto_overworld ();
	}

	public void goto_level(int lvl) {

		// error-ing if menu doesnt exist yet

		menu_script.g_current_level = lvl;

		touchable_buttons = false;

		scene_manager.scene_manager_instance.start_level (lvl);

		//Application.LoadLevel ("DuringGame");
		//game_grid.game_grid_instance.load_level (lvl);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
