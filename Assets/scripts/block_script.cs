﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class block_script : MonoBehaviour {

	public int index = -1;
	public int block_type = 0;
	public int x = -1;
	public int y = -1;
	public GameObject game_state;	// the parent in heirarchy
	public hint_script hint_obj;		// a child in heirarchy
	public counter_script counter_obj;
	public join_sprite_script join_obj;
	public bool covered_up = true;
	public bool flag_on = false;
	public int hint_num = 0;
	public block_script.HintType hint_type = 0;

	public enum HintType {NoHint, FourTouch, EightTouch, Eye, Heart, Compass, Crown, EyeBracket, EyeRepeat, Zap, TotalMines, TotalMineMineContacts};
	// block_script.HintType.Eye

	public Sprite bomb_sprite;
	public Sprite wall_sprite;
	public Sprite cover_sprite;
	public Sprite flag_sprite;

	public Sprite double_cover_sprite;
	public Sprite double_flag_sprite;

	public int mine_multi = 1;

	public int join_group = 0;
	//public int[] share_groups = new int [];
	public List<int> share_groups = new List<int>();
	public List<int> mines_seen_i = new List<int>();
	public bool share_square = false;
	public bool share_pipe = false;

	public bool share_connect_up = false;
	public bool share_connect_down = false;
	public bool share_connect_left = false;
	public bool share_connect_right = false;

	public bool join_leader = false;
	public bool join_second = false;
	public int my_join_leader_x = 0;
	public int my_join_leader_y = 0;
	public bool i_know_my_join_leader = false;

	public List<int> x_in_range = new List<int>();
	public List<int> y_in_range = new List<int>();
	public List<int> i_in_range = new List<int>();

	public int my_vert_seq_length = 0;
	public int my_horiz_seq_length = 0;
	public int my_vert_seq_id = 0;
	public int my_horiz_seq_id = 0;

	// Use this for initialization
	void Start () {
		hint_obj = gameObject.GetComponentInChildren<hint_script> ();
		counter_obj = gameObject.GetComponentInChildren<counter_script> ();
		join_obj = gameObject.GetComponentInChildren<join_sprite_script> ();

		// also needs a join_sprite - used for blue joiners and pink sharebubbles/sharepipes
	}

	// Update is called once per frame
	//void Update () {
		
	//}

	public void reset () {

		hint_obj = gameObject.GetComponentInChildren<hint_script> ();
		counter_obj = gameObject.GetComponentInChildren<counter_script> ();
		join_obj = gameObject.GetComponentInChildren<join_sprite_script> ();

		block_type = 0;
		join_group = 0;
		mine_multi = 1;
		hint_type = 0;

		take_flag_off ();

		my_join_leader_x = 0;
		my_join_leader_y = 0;
		i_know_my_join_leader = false;
		sec_join_leader_index = -1;
		join_leader = false;
		join_second = false;

		x_in_range = new List<int>();
		y_in_range = new List<int>();
		i_in_range = new List<int>();

		share_groups = new List<int>();
		share_pipe = false;
		share_square = false;

		share_connect_up = false;
		share_connect_down = false;
		share_connect_left = false;
		share_connect_right = false;

		join_obj.reset ();

		join_obj.GetComponent<join_sprite_script>().reset ();
	}

	public void clear_range() {
		x_in_range = new List<int>();
		y_in_range = new List<int>();
		i_in_range = new List<int>();
	}

	public void set_multi(int multi_) {
		mine_multi = multi_;
	}

	public void set_type (int new_type) {
		// 1 is wall, 2 is mine, 0 is empty
		block_type = new_type;

		if (new_type != 0)
			GetComponent<SpriteRenderer> ().enabled = true;

		if (new_type == 1) {
			covered_up = false;
			GetComponent<SpriteRenderer> ().sprite = wall_sprite;
			return;
		} 

		if (covered_up == false) {
			if (new_type == 0) {
				GetComponent<SpriteRenderer> ().enabled = false;	// hide sprite
				// maybe show grid here
				// must show hint sprite & counter sprite



			} else if (new_type == 2) {
				GetComponent<SpriteRenderer> ().sprite = bomb_sprite;
			}
			//hint_obj.GetComponent<SpriteRenderer> ().enabled = true;
		} else if (covered_up == true && flag_on == false) {
			if (mine_multi == 2) GetComponent<SpriteRenderer> ().sprite = double_cover_sprite;
			else  GetComponent<SpriteRenderer> ().sprite = cover_sprite;
			//hint_obj.GetComponent<SpriteRenderer> ().enabled = false;
		} else if (covered_up == true && flag_on == true) {
			if (mine_multi == 2) GetComponent<SpriteRenderer> ().sprite = double_flag_sprite;
			else  GetComponent<SpriteRenderer> ().sprite = flag_sprite;
			//hint_obj.GetComponent<SpriteRenderer> ().enabled = false;
		}

	

	}

	public void toggle_flag() {
		if (this.covered_up == false)
			return;
		if (this.block_type == 1)
			return;

		if (this.flag_on == true)
			this.take_flag_off ();
		else
			this.put_flag_on ();

		set_type (block_type);
	}

	void put_flag_on () {
		this.flag_on = true;
	}

	void take_flag_off () {
		this.flag_on = false;
	}

	public void cover () {
		covered_up = true;
		set_type (block_type);

		if (mine_multi == 1) {
			//GetComponent<SpriteRenderer> ().sprite = 
		}

		GetComponent<SpriteRenderer> ().enabled = true;	// cover sprite

		hint_obj = gameObject.GetComponentInChildren<hint_script> ();
		hint_obj.show_hint (0);	// hide hint

		counter_obj = gameObject.GetComponentInChildren<counter_script> ();
		counter_obj.hide ();
	}

	public void link_leader_hint_to_sharegroup(int s_group) {
		share_groups.Add (s_group);
	}

	public void link_hint_to_sharegroup(int s_group) {
		share_groups.Add (s_group);

		if (join_group != 0 &&
		    i_know_my_join_leader == true &&
			join_leader == false) {
			block_script my_leader = game_grid.game_grid_instance.get_block (my_join_leader_x, my_join_leader_y);
			my_leader.link_leader_hint_to_sharegroup (s_group);
		}
	}

	public void include_range_of_joined_tile (int otherx, int othery) {
		
	}



	public void calc_joiner_sprite() {
		if (join_group > 0) {
			
			if (x > 0 && game_grid.game_grid_instance.get_join_group (x - 1, y) == join_group)
				this.join_obj.join_left = true;

			if (x < game_grid.grid_w - 1 && game_grid.game_grid_instance.get_join_group (x + 1, y) == join_group)
				this.join_obj.join_right = true;

			if (y > 0 && game_grid.game_grid_instance.get_join_group (x, y - 1) == join_group)
				this.join_obj.join_up = true;

			if (y < game_grid.grid_h - 1 && game_grid.game_grid_instance.get_join_group (x, y + 1) == join_group)
				this.join_obj.join_down = true;

			join_obj.calc_join_sprite ();
			
		}
	}

	public void get_range_for_joined () {
		if (this.join_leader == false) return;

		calc_hint (); // wipes and re calcs range

		// now combine the ranges of all in this group
		for (int x = 0; x < game_grid.grid_w; x++) {
			for (int y = 0; y < game_grid.grid_h; y++) {
				//block_script block_ = game_grid.game_grid_instance.blocks [x] [y];
				block_script block_ = game_grid.game_grid_instance.get_block (x, y);
				if (block_.join_group == this.join_group &&
					(block_.x != this.x || block_.y != this.y)) {
					int otherx = block_.x;
					int othery = block_.y;
					block_.hint_type = this.hint_type;
					block_.calc_hint();	// stores range	

					// so... calc_hint on join leader, wipes range...

					this.include_range_of_joined_tile(otherx, othery);

				}

			}
		}

	}

	public void identify_mines_in_range () {

		if (this.join_group != 0 && this.join_leader == true) this.get_range_for_joined();

		mines_seen_i = new List<int> ();

		for (int r = 0; r < i_in_range.Count; r++) {
			int block_i = i_in_range [r];

			if (game_grid.game_grid_instance.get_block_type_by_index (block_i) != 2)
				continue;

			int xx = game_grid.game_grid_instance.get_block_x_by_index (block_i);
			int yy = game_grid.game_grid_instance.get_block_y_by_index (block_i);

			// for hearts that only see lonelys
			// kings only biggest
			if (can_i_actually_see (xx,yy) == false)
				continue;

			mines_seen_i.Add (block_i);
		}

		Debug.Log ("identify mines in range > " + mines_seen_i.Count);
	}

	public bool can_i_actually_see(int xx, int yy) {
		if (hint_type == block_script.HintType.Heart) {
			if (game_grid.game_grid_instance.is_mine_lonely (xx, yy) == true)
				return true;
			return false;
		} else if (hint_type == block_script.HintType.Crown) {
			// will only execute once per level after a resrt
			game_grid.game_grid_instance.calc_seq_lengths ();

			if (x != xx && y == yy &&
			    game_grid.game_grid_instance.get_horiz_seq_length (xx, yy) == hint_num)
				return true;
			else if (x == xx && y != yy &&
			         game_grid.game_grid_instance.get_vert_seq_length (xx, yy) == hint_num)
				return true;
			else
				return false;
		}

		return true;	// usual case
	}

	public void uncover_shared() {
		// if sharesquare == true || share_pipe == true then join_sprite_any.make_vis

		take_flag_off ();
		if (block_type == 1)
			return;
		// deselect
		covered_up = false;
		flag_on = false;
		set_type (block_type);

		if (hint_type != block_script.HintType.NoHint) {
			calc_hint ();

			//show_hint ();

			identify_mines_in_range ();
		} 

		// if sharesquare == false then show_share_square
		// shows both sharepipe and sharesquare
		// wait to show actual sharesquare until its all calculated
		if (share_square == false) show_sharesquare();
	}

	public void uncover_joined () {

		Debug.Log ("uncover joined");
		
		// join_sprite_any.make_vis
		// when is join_sprite_any calculated?
		join_obj.make_vis();

		take_flag_off ();
		if (block_type == 1)
			return;
		// deselect
		covered_up = false;
		flag_on = false;
		set_type (block_type);
	}

	public int sec_join_leader_index = 0;

	private void uncover_joined_first () {
		// find the 'leader block' of this group
		// my_join_leader_x / y


		// find any other of this group -> uncover_joined
		game_grid.game_grid_instance.uncover_joined_group(join_group);

		// if any other of this group knows our hint_type, remember it - all hint types set in load_level

		// tell the leader to calc_hint (I think just to get its range)
		// this can return self
		block_script leader_b = game_grid.game_grid_instance.get_block(my_join_leader_x, my_join_leader_y);
		leader_b.calc_hint ();

		int sec_join_leader_index = leader_b.sec_join_leader_index;
		block_script sec_leader_b = game_grid.game_grid_instance.get_block_by_index(sec_join_leader_index);

		// now combine the ranges of all in this group into the leader

		for (int x = 0; x < game_grid.grid_w; x++) {
			for (int y = 0; y < game_grid.grid_h; y++) {
				block_script block_ = game_grid.game_grid_instance.get_block (x, y);
				if (block_.join_group != join_group)
					continue;
				if (block_.join_leader == true)
					continue;

				block_.calc_hint ();	// calcs range

				// might get tiles inside the blue group - does it matter?
				for (int r = 0; r < block_.x_in_range.Count; r++) {
					int xx = block_.x_in_range [r];
					int yy = block_.y_in_range [r];
					leader_b.add_to_range (xx, yy);
				}
			}
		}



		// call .include_range_of_joined_tile on leader

		// there must be a secondary leader, just to show the symbol & number on 2 tiles
		// (setup in load_level)


		// hearts cant see doublemines - turn hearts into eyes to avoid this - if (hint_type == HEART)
		if (hint_type == HintType.Heart) {
			bool seen_double = false;
			for (int r = 0; r < leader_b.x_in_range.Count; r++) {
				int xxx = leader_b.x_in_range [r];
				int yyy = leader_b.y_in_range [r];
				int multi_ = game_grid.game_grid_instance.get_multi (xxx,yyy);

				if (multi_ > 1) {
					leader_b.preset_hint (HintType.Eye);
					//leader_b.cover ();
					//leader_b.uncover ();
				}
			}
		}

		// for certain hint types we need a different approach - get the max of the individual tiles
		// such as crowns

		// else
		// calc_hint_from_range just counts the mines in range
		// leaderblock.calc_hint_from_range(hint_type)

		leader_b.calc_hint_from_range ();

		//Debug.Log ("uncover joined first leader_b.hint_num " + leader_b.hint_num);

		// why?
		//this.game_state.blocks[leader_b].mines_seen_xy = [];
		//this.game_state.blocks[leader_b].identify_mines_in_range();

		int joined_hint_num = leader_b.hint_num;
		leader_b.hint_obj.show_hint(hint_type);
		sec_leader_b.counter_obj.set_num (joined_hint_num);

	}

	public void uncover () {
		if (covered_up == false)
			return;
		covered_up = false;
		flag_on = false;
		set_type (block_type);

		if (join_group != 0) {
			Debug.Log ("uncover -> join_goup != 0");
			uncover_joined_first ();	// put in another function for cleanliness
			return;
		}

		if (share_groups.Count == 1)
			Debug.Log ("share_groups.Count == 1");

		if (share_groups.Count == 1 && hint_type == block_script.HintType.NoHint) {

			Debug.Log ("block > uncover > pink-shared!");

			// its possible for a hint that belongs to a share clue to be uncovered while the share clue is covered
			// but not vice versa

			int share_group = share_groups [0];
			int sharesq_b = 0;

			for (int xx = 0; xx < game_grid.grid_w; xx++) {
				for (int yy = 0; yy < game_grid.grid_h; yy++) {
					//block_script block_ = game_grid.game_grid_instance.blocks [x] [y];
					block_script block_ = game_grid.game_grid_instance.get_block(xx,yy);
					for (int s = 0; s < block_.share_groups.Count; s++) {
						if (block_.share_groups[s] != share_group) continue;
						block_.uncover_shared ();
						// may be blue-join tiles on the ends of this share-tentacle:
						if (block_.join_group != 0) block_.uncover();
					}

					// find my sharebubble
					if (block_.share_groups.Count == 1 &&
					    block_.share_groups [0] == share_group &&
					    block_.share_square == true)
							sharesq_b = game_grid.game_grid_instance.get_index_of_block (xx,yy);
				}
			}

			game_grid.game_grid_instance.get_block_by_index (sharesq_b).calc_sharesquare ();
			game_grid.game_grid_instance.get_block_by_index (sharesq_b).show_sharesquare ();

			return;

		}

		// calc hint
		calc_hint();

		hint_obj = gameObject.GetComponentInChildren<hint_script> ();
		hint_obj.show_hint (hint_type);

		if (hint_type != 0) {
			counter_obj = gameObject.GetComponentInChildren<counter_script> ();
			counter_obj.set_num (hint_num);
		} else {
			counter_obj = gameObject.GetComponentInChildren<counter_script> ();
			counter_obj.hide ();
		}

	}

	private bool shared_crown;
	private bool shared_heart;
	private bool shared_eyebracket;

	public void calc_sharesquare() {
		List<int> all_the_mines = new List<int>();
		int num_hints_in_group = 0;
		List<int> hints_in_this_sharesquare = new List<int>();

		shared_crown = false;
		shared_heart = false;
		shared_eyebracket = false;

		for (int x = 0; x < game_grid.grid_w; x++) {
			for (int y = 0; y < game_grid.grid_h; y++) {
				block_script block_ = game_grid.game_grid_instance.get_block (x, y);
				if (block_.hint_type == block_script.HintType.NoHint)
					continue;

				int not_in_my_group = 1;

				for (var s = 0; s < block_.share_groups.Count; s++) {
					if (block_.share_groups [s] == share_groups [0])
						not_in_my_group = 0;	// so it is in my group
				}

				if (not_in_my_group == 1)
					continue;

				hints_in_this_sharesquare.Add(block_.index);

				if (block_.hint_type == block_script.HintType.Crown)
					shared_crown = true;
				else if(block_.hint_type == block_script.HintType.Heart)
					shared_heart = true;
				else if(block_.hint_type == block_script.HintType.EyeBracket)
					shared_eyebracket = true;

				num_hints_in_group++;

				for (int m = 0; m < block_.mines_seen_i.Count; m++) {
					all_the_mines.Add(block_.mines_seen_i[m]);
				}

			}
		}

		int shared_ = 0;

		Debug.Log ("calc sharesquaare > num_hints_in_group = " + num_hints_in_group);
		Debug.Log ("all_the_mines.Count " + all_the_mines.Count);


		// looks for mines in all_the_mines that are counted 2x
		for (int m = 0; m < all_the_mines.Count; m++) {
			int num_of_m = 1;	// evry mine is counted at least once

			for (int n = m + 1; n < all_the_mines.Count; n++) {
				if (all_the_mines [m] == all_the_mines [n])
					num_of_m++;
			}

			// how many mines in this tile?
			int multi = game_grid.game_grid_instance.get_block_by_index(all_the_mines[m]).mine_multi;

			Debug.Log ("calc sharesquaare > multi = " + multi);

			if (num_of_m == num_hints_in_group)
				shared_ += multi;

		}

		sharesquare_num = shared_;
	}

	public bool is_in_sharegroup(int s_group) {
		for (int s = 0; s < share_groups.Count; s++) {
			if (share_groups [s] == s_group)
				return true;
		}

		return false;
	}


	private void show_sharesquare() {
		Debug.Log ("block > fn show_sharesquare");
		if (share_groups.Count == 0 ||
			join_group != 0 ||
			hint_type != 0)
			return;

		// join_sprite_any - center x, y + 1
		// I dont think the offset of join_sprite ever changes

		join_obj.reset ();

		if (share_square == true) {

			// HTML5 version has a bit more double checking to make sure we
			//	are connected to lefr/right/u/d
			// 	but this was already calculated in load_level

			if (share_connect_left && x == 0)
				share_connect_left = false;
			if (share_connect_right && x == game_grid.game_grid_instance.get_grid_w() - 1)
				share_connect_right = false;
			if (share_connect_up && y == 0)
				share_connect_up = false;
			if (share_connect_down && y == game_grid.game_grid_instance.get_grid_h() - 1)
				share_connect_down = false;

			int s_group = share_groups [0];

			if (share_connect_left &&
				x > 0 &&
				game_grid.game_grid_instance.get_block(x-1,y).is_in_sharegroup(s_group) == false) share_connect_left = false;

			if (share_connect_right &&
				x < game_grid.game_grid_instance.get_grid_w() - 1 &&
				game_grid.game_grid_instance.get_block(x+1,y).is_in_sharegroup(s_group) == false) share_connect_right = false;

			if (share_connect_up &&
				y > 0 &&
				game_grid.game_grid_instance.get_block(x,y - 1).is_in_sharegroup(s_group) == false) share_connect_up = false;


			if (share_connect_down &&
				y < game_grid.game_grid_instance.get_grid_h() - 1 &&
				game_grid.game_grid_instance.get_block(x,y + 1).is_in_sharegroup(s_group) == false) share_connect_down = false;
		}

		bool L = share_connect_left;
		bool R = share_connect_right;
		bool D = share_connect_down;
		bool U = share_connect_up;




		join_obj.join_up = U;
		join_obj.join_down = D;
		join_obj.join_left = L;
		join_obj.join_right = R;

		if (share_square) {
			Debug.Log ("block > show share bubble x: " + x + " y: " + y);
			join_obj.calc_sharesquare ();

			Debug.Log ("sharesquare_num " + sharesquare_num);
			//counter_obj = gameObject.GetComponentInChildren<counter_script> ();
			counter_obj.set_num (sharesquare_num);

			if (shared_crown) {

			}



		} else if (share_pipe) {
			Debug.Log ("block > show share pipe x: " + x + " y: " + y);
			join_obj.calc_sharepipe ();
		}

		join_obj.make_vis ();
	}

	public bool flowfill_seen = false;

	// for the zap hint
	// the range is unknown to the player
	private void calc_zap () {
		game_grid.game_grid_instance.reset_flowfill ();

		Stack <int> fringe = new Stack<int>();
		int loops_ = 0;

		fringe.Push (index);
		game_grid.game_grid_instance.mark_flowfill_seen (index);

		x_in_range = new List<int> ();
		y_in_range = new List<int> ();
		i_in_range = new List<int> ();

		int num_mines_zap = 0;

		while (fringe.Count > 0 && loops_ < 110) {
			loops_++;

			int b = fringe.Pop ();

			int xx = game_grid.game_grid_instance.get_block_x_by_index (b);
			int yy = game_grid.game_grid_instance.get_block_y_by_index (b);

			num_mines_zap += get_mines_at(xx, yy);

			add_to_range (xx, yy);

			// left
			if (xx > 0 &&
			    get_mines_at (xx - 1, yy) > 0 &&
			    game_grid.game_grid_instance.is_flowfill_seen (xx - 1, yy) == false) {
					int new_b = game_grid.game_grid_instance.get_index_of_block (xx - 1, yy);
					fringe.Push (new_b);
					game_grid.game_grid_instance.mark_flowfill_seen (new_b);
			}

			// right
			if (xx < game_grid.game_grid_instance.get_grid_w() - 1 &&
				get_mines_at (xx + 1, yy) > 0 &&
				game_grid.game_grid_instance.is_flowfill_seen (xx + 1, yy) == false) {
				int new_b = game_grid.game_grid_instance.get_index_of_block (xx + 1, yy);
				fringe.Push (new_b);
				game_grid.game_grid_instance.mark_flowfill_seen (new_b);
			}

			// down
			if (yy < game_grid.game_grid_instance.get_grid_h() - 1 &&
				get_mines_at (xx, yy + 1) > 0 &&
				game_grid.game_grid_instance.is_flowfill_seen (xx, yy + 1) == false) {
				int new_b = game_grid.game_grid_instance.get_index_of_block (xx, yy + 1);
				fringe.Push (new_b);
				game_grid.game_grid_instance.mark_flowfill_seen (new_b);
			}

			// up
			if (yy > 0 &&
				get_mines_at (xx, yy - 1) > 0 &&
				game_grid.game_grid_instance.is_flowfill_seen (xx, yy - 1) == false) {
				int new_b = game_grid.game_grid_instance.get_index_of_block (xx, yy - 1);
				fringe.Push (new_b);
				game_grid.game_grid_instance.mark_flowfill_seen (new_b);
			}
		}

		hint_num = num_mines_zap;
	}

	private int sharesquare_num = 99;

	public int get_num_mines () {
		if (block_type != 2)
			return 0;
		return mine_multi;
	}

	private void add_to_range(int xx, int yy) {

		if (xx < 0 || yy < 0 || xx > game_grid.grid_w - 1 || yy > game_grid.grid_h - 1)
			return;

		// is it already in our range? avoid duplicates
		bool already_in = false;
		for (int r = 0; r < x_in_range.Count; r++) {
			if (xx == x_in_range [r] && yy == y_in_range [r])
				already_in = true;
		}

		if (already_in == true)
			return;

		int other_index = game_grid.game_grid_instance.get_index_of_block (xx,yy);

		x_in_range.Add (xx);
		y_in_range.Add (yy);
		i_in_range.Add (other_index);
	}


	public void fill_range_line_of_sight(){
		// self ?
		// add_to_range(x, y);

		// up
		for (int yy = y - 1; yy >= 0; yy--) {
			if (game_grid.game_grid_instance.get_block_type (x, yy) == 1)
				break;
			add_to_range(x, yy);
		}

		// down
		for (int yy = y + 1; yy <= game_grid.grid_h - 1; yy++) {
			if (game_grid.game_grid_instance.get_block_type (x, yy) == 1)
				break;
			add_to_range(x, yy);
		}


		// left
		for (int xx = x - 1; xx >= 0; xx--) {
			if (game_grid.game_grid_instance.get_block_type (xx, y) == 1)
				break;
			add_to_range(xx, y);
		}


		// right
		for (int xx = x + 1; xx <= game_grid.grid_w - 1; xx++) {
			if (game_grid.game_grid_instance.get_block_type (xx, y) == 1)
				break;
			add_to_range(xx, y);
		}
	}

	public void calc_hint_from_range() {
		if (hint_type == block_script.HintType.Crown) {
			// I think this only gets used if I make joined crown tiless
		} if (hint_type == block_script.HintType.EyeBracket) {

		} else {

			hint_num = 0;

			for (int i = 0; i < x_in_range.Count; i++) {
				int xx = x_in_range [i];
				int yy = y_in_range [i];

				if (hint_type == block_script.HintType.Heart) {
					hint_num += get_lonely_mines_at (xx, yy);
				} else {

					hint_num += get_mines_at (xx, yy);
				}
			}

		}
	}

	private void calc_eyebracket() {
		int num_groups = 0;
		bool new_group = true;

		// up
		for (int yy = y - 1; yy >= 0; yy--) {
			if (game_grid.game_grid_instance.get_block_type (x, yy) == 1)
				break;
			add_to_range(x, yy);

			if (game_grid.game_grid_instance.get_block_type (x, yy) == 2 &&
			    new_group == true) {
				num_groups++;
				new_group = false;
			} else if (game_grid.game_grid_instance.get_block_type (x, yy) == 0) {
				new_group = true;
			}



		}

		new_group = true;

		// down
		for (int yy = y + 1; yy <= game_grid.grid_h - 1; yy++) {
			if (game_grid.game_grid_instance.get_block_type (x, yy) == 1)
				break;
			add_to_range(x, yy);

			if (game_grid.game_grid_instance.get_block_type (x, yy) == 2 &&
				new_group == true) {
				num_groups++;
				new_group = false;
			} else if (game_grid.game_grid_instance.get_block_type (x, yy) == 0) {
				new_group = true;
			}
		}

		new_group = true;
		// left
		for (int xx = x - 1; xx >= 0; xx--) {
			if (game_grid.game_grid_instance.get_block_type (xx, y) == 1)
				break;
			add_to_range(xx, y);

			if (game_grid.game_grid_instance.get_block_type (xx, y) == 2 &&
				new_group == true) {
				num_groups++;
				new_group = false;
			} else if (game_grid.game_grid_instance.get_block_type (xx, y) == 0) {
				new_group = true;
			}
		}

		new_group = true;
		// right
		for (int xx = x + 1; xx <= game_grid.grid_w - 1; xx++) {
			if (game_grid.game_grid_instance.get_block_type (xx, y) == 1)
				break;
			add_to_range(xx, y);

			if (game_grid.game_grid_instance.get_block_type (xx, y) == 2 &&
				new_group == true) {
				num_groups++;
				new_group = false;
			} else if (game_grid.game_grid_instance.get_block_type (xx, y) == 0) {
				new_group = true;
			}
		}

		hint_num = num_groups;
	}

	public void calc_eyerepeat() {
		game_grid.game_grid_instance.reset_flowfill ();

		x_in_range = new List<int> ();
		y_in_range = new List<int> ();
		i_in_range = new List<int> ();

		fill_range_line_of_sight ();

		int max_steps = 2;

		Stack<int> fringe_ = new Stack<int> ();

		for (int i = 0; i < i_in_range.Count; i++) {
			fringe_.Push (i_in_range [i]);
		}

		x_in_range = new List<int> ();
		y_in_range = new List<int> ();
		i_in_range = new List<int> ();

		int loops_ = 0;

		while(loops_ < 100 && fringe_.Count > 0) {
			loops_++;

			int b = fringe_.Pop ();

			int xx = game_grid.game_grid_instance.get_block_x_by_index (b);
			int yy = game_grid.game_grid_instance.get_block_y_by_index (b);

			add_to_range (xx, yy);
			game_grid.game_grid_instance.mark_flowfill_seen (b);

			int type_ = game_grid.game_grid_instance.get_block_type (xx, yy);

			if (type_ == 2) {
				if (xx == x) {


					// left
					for (int xxx = xx - 1; xxx >= 0; xxx--) {
						if (game_grid.game_grid_instance.get_block_type (xxx, yy) == 1)
							break;
						add_to_range(xxx, yy);
					}


					// right
					for (int xxx = x + 1; xxx <= game_grid.grid_w - 1; xxx++) {
						if (game_grid.game_grid_instance.get_block_type (xxx, yy) == 1)
							break;
						add_to_range(xxx, yy);
					}

				} else if (yy == y) {
					// up
					for (int yyy = yy - 1; yyy >= 0; yyy--) {
						if (game_grid.game_grid_instance.get_block_type (xx, yyy) == 1)
							break;
						add_to_range(xx, yyy);
					}

					// down
					for (int yyy = yy + 1; yyy <= game_grid.grid_h - 1; yyy++) {
						if (game_grid.game_grid_instance.get_block_type (xx, yyy) == 1)
							break;
						add_to_range(xx, yyy);
					}
				}
			}

		}

		calc_hint_from_range ();

	}

	// 1 -is 4 touch
	// 2 is eye
	public void calc_hint () {
		hint_num = 0;
		if (hint_type == block_script.HintType.FourTouch) {

			add_to_range (x - 1, y);
			add_to_range (x + 1, y);
			add_to_range (x, y + 1);
			add_to_range (x, y - 1);

			//game_state.g
			int four_touch = 0;
			four_touch += get_mines_at (this.x - 1, this.y);
			four_touch += get_mines_at (this.x + 1, this.y);
			four_touch += get_mines_at (this.x, this.y - 1);
			four_touch += get_mines_at (this.x, this.y + 1);
			hint_num = four_touch;
			Debug.Log ("four touch -> " + hint_num);
		} else if (hint_type == block_script.HintType.EightTouch) {

			add_to_range (x - 1, y);
			add_to_range (x + 1, y);
			add_to_range (x, y + 1);
			add_to_range (x, y - 1);

			add_to_range (x - 1, y - 1);
			add_to_range (x + 1, y + 1);
			add_to_range (x - 1, y + 1);
			add_to_range (x + 1, y - 1);

			//game_state.g
			calc_hint_from_range ();
		} else if (hint_type == block_script.HintType.Eye) {
			fill_range_line_of_sight ();
			calc_hint_from_range ();

		} else if (hint_type == block_script.HintType.Heart) {
			fill_range_line_of_sight ();
			calc_hint_from_range ();

		} else if (hint_type == block_script.HintType.Zap) {
			calc_zap();
		} else if (hint_type == block_script.HintType.EyeRepeat) {
			calc_eyerepeat();
		} else if (hint_type == block_script.HintType.Crown) {
			fill_range_line_of_sight ();

			int best = 0;

			for (int r = 0; r < x_in_range.Count; r++) {

				int vert_len = game_grid.game_grid_instance.get_vert_seq_length (x_in_range [r], y_in_range [r]);
				int horiz_len = game_grid.game_grid_instance.get_horiz_seq_length (x_in_range [r], y_in_range [r]);

				if (x_in_range [r] == this.x &&
					vert_len > best) {
					best = vert_len;
				} else if (y_in_range[r] == this.y &&
					horiz_len > best) {
					best = horiz_len;
				}
			}

			hint_num = best;

		} else if (hint_type == block_script.HintType.EyeBracket) {
			calc_eyebracket ();

		} else if (hint_type == block_script.HintType.Compass) {
			fill_range_line_of_sight ();	// do we need this?
			int directions = 0;

			bool above = false; 
			bool below = false;
			bool left = false; 
			bool right = false;

			for (int r = 0; r < i_in_range.Count; r++) {
				int xx = x_in_range [r];
				int yy = y_in_range [r];

				if (get_mines_at (xx, yy) == 0)
					continue;

				if (xx == x) {
					if (yy > y)
						below = true;
					else
						above = true;
				} else if (yy == y) {
					if (xx > x)
						right = true;
					else
						left = true;
				}
			} // for r in range

			if (above)
				directions++;
			if (below)
				directions++;
			if (left)
				directions++;
			if (right)
				directions++;

			hint_num = directions;
		}
	}

	public void preset_hint (block_script.HintType hint) {
		hint_type = hint;
	}

	// calc what is there vs. what the player says is there
	// for calculating hint satisfaction (to grey-out) it needs 2 conditions
	// (1) whole range cleared
	// (2) needs to calculate the same in both pixel_modes
	//	 	(as in Pixoji - I'll have a dummy block object that mimics then compares)
	//		unflagged would be 0, same as an uncovered - then I think hearts would grey out properly?
	//   	in MoS HTML5 I used custom functions for the heart and the zap
	private int pixel_mode = 1;
	int get_mines_at (int xx, int yy) {
		
		if (pixel_mode == 1) {
			// count actual mines
			//return game_state.get_pixel(x,y);
			//GetComponentInParent<game_grid>().get_num_mines (xx, yy);
			return game_grid.game_grid_instance.get_num_mines (xx, yy);
		} else {
			// count user flags
			//return game_state.get_player_pixel(x,y);
			return GetComponentInParent<game_grid>().get_num_player_flags (xx, yy);
		}
		return 0;//game_state.get_num_mines (x, y);
	}

	int get_lonely_mines_at(int xx, int yy) {
		if (pixel_mode == 1) {
			// count actual mines
			//return game_state.get_pixel(x,y);
			//GetComponentInParent<game_grid>().get_num_mines (xx, yy);
			if (game_grid.game_grid_instance.is_mine_lonely(xx, yy) == false) return 0;
			return game_grid.game_grid_instance.get_num_mines (xx, yy);
		} else {
			// count user flags
			//return game_state.get_player_pixel(x,y);
			if (game_grid.game_grid_instance.is_flag_lonely(xx, yy) == false) return 0;
			return game_grid.game_grid_instance.get_num_player_flags (xx, yy);
		}
		return 0;//game_state.get_num_mines (x, y);
	}

	// used only for the test clue
	void mimic (GameObject other_guy) {
		//x = other_guy.x;
		//y = other_guy.y;
		//block_type = other_guy.block_type;
		//hint_type = other_guy.hint_type;
		//flag_on = other_guy.flag_on;
	}
		
	// used only for the test clue
	bool compare (GameObject other_guy) {
		
		//if (hint_num == other_guy.hint_num)
		//	return true;
		//else
		//	return false;
		return true;
	}




	public void OnMouseDown () {

		Debug.Log ("block_script > onmousedown x: " + x + " y " + y);

		game_grid parentScript = this.transform.parent.GetComponent<game_grid>();
		parentScript.tile_clicked (this.x, this.y);

		//game_state.GetComponent<game_grid>.tile_clicked(this.x, this.y);
		
		//GetComponentInParent<game_grid>.tile_clicked (this.x, this.y);

		// game_grid.GetComponent<game_grid>().get_grid_w()

	}

	//public void dig () {}

	//public void flag () {}

}
