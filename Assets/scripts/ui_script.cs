﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ui_script : MonoBehaviour {

	public static ui_script ui_instance;
	public UnityEngine.UI.Button corner_button; 
	public menu_script side_menu;
	public RectTransform side_menu_rect_tansform;
	public action_buttons_script action_buttons;
	public gamepad gamepad_object_script;
	public UnityEngine.UI.Button next_level_button;
	public UnityEngine.UI.Button redo_level_button;

	public UnityEngine.UI.Text text_a;
	public UnityEngine.UI.Text text_b;
	public UnityEngine.UI.Text clue_desc_text;

	private int screen_width = 0;
	private int screen_height = 0;

	public enum ControlRegime {NoControl, TileFirst, ActionFirst};
	public ControlRegime current_control_regime = ControlRegime.TileFirst;

	void Awake () {

		// this would be called at the start of every level

		if (ui_instance == null) {
			ui_instance = this;
			DontDestroyOnLoad (transform.root.gameObject);

			// child objects
			text_a.enabled = false;

			screen_width = Screen.width;
			screen_height = Screen.height;
			on_resize ();

		} else {
			DestroyObject(gameObject);
			return;
		}

		// All Awakes run before any Starts run before any Updates. 

	}

	public void screen_resized() {

	}

	public void on_ingame_end() {
		action_buttons.hide ();
		gamepad_object_script.hide ();
		redo_level_button.gameObject.SetActive (false);
		next_level_button.gameObject.SetActive (false);

		text_a.enabled = false;
		text_b.enabled = false;
		clue_desc_text.enabled = false;
	}

	public void on_ingame_start() {
		// hide text_a, text_b
		// show in game controls
		action_buttons.make_vis ();
		gamepad_object_script.make_vis ();

		text_a.enabled = true;
		text_b.enabled = true;
		clue_desc_text.enabled = true;

		redo_level_button.gameObject.SetActive (false);
		next_level_button.gameObject.SetActive (false);
	}

	public void on_lose_start() {
		redo_level_button.gameObject.SetActive (true);
	}

	public void on_win_start() {

		next_level_button.gameObject.SetActive (true);
	}

	public void click_next_level() {
		game_grid.game_grid_instance.goto_next_level ();
		on_ingame_end ();
	}

	public void click_redo_level() {
		game_grid.game_grid_instance.retry_level ();
		on_ingame_end ();
	}



	public void on_overworld_start() {
		on_ingame_end ();
	}

	public void hide_ingame_text () {
		Debug.Log ("ui_script > hide ingame text");
		text_a.text = "";
		//text_a.GetComponent<Text> ().active = false;
		text_a.enabled = false;

		text_b.text = "";
		text_b.enabled = false;

		clue_desc_text.text = "";
		clue_desc_text.enabled = false;
	}

	public void set_ingame_text(string str_to_show, string str_b_to_show = "") {
		Debug.Log ("ui_script > show ingame text > " + str_to_show);
		text_a.text = str_to_show;
		text_a.enabled = true;
		//text_a.GetComponent<Text> ().enabled = true;

		text_b.text = str_b_to_show;
		text_b.enabled = true;


	}

	// Use this for initialization
	void Start () {
		
	}

	public void describe_hint(block_script.HintType hint_) {
		if (hint_ == block_script.HintType.Eye) {
			clue_desc_text.text = "The eye counts mines in its line of sight. Blocked by walls.";
		} else if (hint_ == block_script.HintType.Heart) {
			clue_desc_text.text = "Like the eye, but only sees LONELY mines. Lonely mines touch no other mine on four sides.";
		}
	}

	public void on_resize () {
		
		//gameObject.GetComponent<RectTransform>().s
		//Width = screen_width;
		gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2((float)Screen.width, (float)Screen.height);

		action_buttons.on_resize ();

		side_menu.transform.position = new Vector2 (0, screen_height - 759);	// menu height is 759
		// w: 199	h:759
		// portrait: width should be ~66% of screen width
		// land:	width should be ~ 
		float oldmenu_w = 199f;
		float oldmenu_h = 759f;
		float menu_w = 199f;
		float menu_h = 759f;
		float menu_ratio = menu_w / menu_h;
		if (Screen.width < Screen.height) {
			menu_w = (float)Screen.width * 0.5f;
			if (menu_w < 160f)
				menu_w = 160f;	
		
			menu_h = menu_w / menu_ratio;
			menu_h = Mathf.Max ((float)Screen.height, menu_h);	// sometimes the screen is too tall
		} else {
			menu_h = (float)Screen.height * 1.5f;	
			menu_w = menu_h * menu_ratio;
		}


		side_menu_rect_tansform.localScale = new Vector2 (menu_w/oldmenu_w, menu_h/oldmenu_h);
		//side_menu_rect_tansform.sizeDelta = new Vector2 (menu_w, menu_h);
		side_menu.set_width((int)menu_w);	// how far to pop out
		side_menu_rect_tansform.position = new Vector2 (0, -menu_h + (float)Screen.height);

		//side_menu.transform.localPosition = new Vector2 (0, -menu_h);	// relative to parent ui_container
		//side_menu.transform.localPosition = new Vector2 (0, 0);	// relative to parent ui_container

		text_a.transform.position = new Vector3 ((float)screen_width*0.5f, (float)screen_height - 33f, 0f); // why does swapping x and y work
		text_a.fontSize = 14;//(int)(screen_width*0.025);
		text_a.rectTransform.sizeDelta = new Vector2((float)screen_width, 66f);//screen_width;

		text_b.transform.position = new Vector3 ((float)screen_width*0.5f, (float)screen_height - 52f, 0f); // why does swapping x and y work
		text_b.fontSize = 12;//(int)(screen_width*0.025);
		text_b.rectTransform.sizeDelta = new Vector2((float)screen_width, 66f);//screen_width;

		clue_desc_text.transform.position = new Vector2 ((float)screen_width * 0.5f, 10f);
		clue_desc_text.fontSize = 10;

		float old_corner_button_size = 25f;
		//float target_size = 
		//float gui_scale = 
		//corner_button.transform.localScale = new Vector2 ();

		corner_button.transform.position = new Vector3 (25, 25, 0);

		redo_level_button.transform.position = new Vector2 (screen_width - 26, screen_height - 26);
		next_level_button.transform.position = new Vector2 (screen_width - 26, screen_height - 26);
	}
	
	// Update is called once per frame
	void Update () {
		if (screen_width != Screen.width ||
			screen_height != Screen.height) {

			screen_width = Screen.width;
			screen_height = Screen.height;
			on_resize ();
		}
	}
}
