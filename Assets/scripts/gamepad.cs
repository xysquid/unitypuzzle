﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine;

public class gamepad : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler {

	// adapted from https://www.youtube.com/watch?v=6kGxSS66Ba8


	//private Image left_arrow;
	//private Image up_arrow;
	//private Image down_arrow;
	//private Image right_arrow;

	public int x_dir = 0;
	public int y_dir = 0;

	private bool pointer_is_down = false;
	private float hold_time = 0f;

	// Use this for initialization
	void Start () {
		//left_arrow = GetComponentInChildren<Image> ();
	}

	private float prev_frame_time = 0;

	// Update is called once per frame
	void Update () {
		if (pointer_is_down == true) {
			float time_passed = Time.time - prev_frame_time;
			prev_frame_time = Time.time;
			hold_time += prev_frame_time; 

			if (hold_time > 1000f) {
				hold_time = 0;
				game_grid.game_grid_instance.move_cursor (x_dir, y_dir);
			}
		}


	}


	public void hide() {
		gameObject.SetActive (false);
	}

	public void make_vis() {
		gameObject.SetActive (true);
	}

	// do I need dragging?
	public virtual void OnDrag(PointerEventData ped) {
		//Debug.Log ("gamepad > onpointerdrag");

	}

	public virtual void OnPointerDown (PointerEventData ped) {
		//Vector2 gamepad_pos = ped.position - transform.position;

		float x_pos = ped.position.x - transform.position.x;
		float y_pos = ped.position.y - transform.position.y;



		float up_off = Mathf.Abs(x_pos - 0) + Mathf.Abs(y_pos - 42);
		float down_off = Mathf.Abs(x_pos - 42) + Mathf.Abs(y_pos - 0);
		float left_off = Mathf.Abs(x_pos - 0) + Mathf.Abs(y_pos - - 42);
		float right_off = Mathf.Abs(x_pos - - 42) + Mathf.Abs(y_pos - 0);

		if (up_off > 48 && left_off > 48 && down_off > 48 && right_off > 48) return;

		x_dir = 0;
		y_dir = 0;

		if (up_off < down_off &&
		    up_off < left_off &&
		    up_off < right_off)
			y_dir = 1;
		else if (down_off < left_off &&
			down_off < right_off) x_dir = 1;
		else if (left_off < right_off) y_dir = -1;
		else x_dir = -1;

		Debug.Log ("gamepad > onpointerdown x: " + x_pos + " y: " + y_pos);
		game_grid.game_grid_instance.move_cursor (x_dir, y_dir);
		pointer_is_down = true;
		hold_time = 0f;
		prev_frame_time = Time.time;
	}

	public virtual void OnPointerUp (PointerEventData ped) {
		Debug.Log ("gamepad > onpointerup");
		pointer_is_down = false;
		x_dir = 0;
		y_dir = 0;
	}
}
