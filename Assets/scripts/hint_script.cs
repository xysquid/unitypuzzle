﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hint_script : MonoBehaviour {

	public Sprite eye_sprite;
	public Sprite hand_sprite;
	public Sprite eight_sprite;
	public Sprite heart_sprite;
	public Sprite crown_sprite;
	public Sprite eyerepeat_sprite;
	public Sprite zap_sprite;
	public Sprite eyebracket_sprite;
	public Sprite totalmines_sprite;
	public Sprite totaladjacentmines_sprite;
	public Sprite compass_sprite;

	//public counter_script counter_obj;

	//public Text hint_num;

	//private GameObject counter_obj;// = new GameObject;
	//public GameObject counter_prefab;

	// Use this for initialization
	void Awake () {
		//counter_obj = Instantiate(counter_prefab, new Vector2 (0, 0), Quaternion.identity) as GameObject;
			//
		//gameObject.GetComponentInChildren<counter_script> ();
	}

	void Start () {
		
	}
	
	// Update is called once per frame
	//void Update () {
		
	//}



	public void show_hint(block_script.HintType hint_type) {



		if (hint_type == block_script.HintType.NoHint) {
			GetComponent<SpriteRenderer> ().enabled = false;

			//counter_obj.GetComponent<counter_script>().hide ();
		} else {
			//counter_obj.make_vis ();
			//counter_obj.GetComponent<counter_script>().set_num(2);
		}

		if (hint_type == block_script.HintType.FourTouch) {
			GetComponent<SpriteRenderer> ().sprite = hand_sprite;
			GetComponent<SpriteRenderer> ().enabled = true;

		} else if (hint_type == block_script.HintType.Eye) {
			
			GetComponent<SpriteRenderer> ().sprite = eye_sprite;
			GetComponent<SpriteRenderer> ().enabled = true;
		} else if (hint_type == block_script.HintType.EightTouch) {
			GetComponent<SpriteRenderer> ().sprite = eight_sprite;
			GetComponent<SpriteRenderer> ().enabled = true;
		} else if (hint_type == block_script.HintType.Heart) {
			GetComponent<SpriteRenderer> ().sprite = heart_sprite;
			GetComponent<SpriteRenderer> ().enabled = true;
		} else if (hint_type == block_script.HintType.Crown) {
			GetComponent<SpriteRenderer> ().sprite = crown_sprite;
			GetComponent<SpriteRenderer> ().enabled = true;
		} else if (hint_type == block_script.HintType.Compass) {
			GetComponent<SpriteRenderer> ().sprite = compass_sprite;
			GetComponent<SpriteRenderer> ().enabled = true;
		} else if (hint_type == block_script.HintType.EyeBracket) {
			GetComponent<SpriteRenderer> ().sprite = eyebracket_sprite;
			GetComponent<SpriteRenderer> ().enabled = true;
		} else if (hint_type == block_script.HintType.Zap) {
			GetComponent<SpriteRenderer> ().sprite = zap_sprite;
			GetComponent<SpriteRenderer> ().enabled = true;
		} else if (hint_type == block_script.HintType.EyeRepeat) {
			GetComponent<SpriteRenderer> ().sprite = eyerepeat_sprite;
			GetComponent<SpriteRenderer> ().enabled = true;
		} else if (hint_type == block_script.HintType.TotalMines) {
			GetComponent<SpriteRenderer> ().sprite = totalmines_sprite;
			GetComponent<SpriteRenderer> ().enabled = true;
		} else if (hint_type == block_script.HintType.TotalMineMineContacts) {
			GetComponent<SpriteRenderer> ().sprite = totaladjacentmines_sprite;
			GetComponent<SpriteRenderer> ().enabled = true;
		}
	}




}
