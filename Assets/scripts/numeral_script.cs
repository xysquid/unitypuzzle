﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class numeral_script : MonoBehaviour {

	/*
	public Sprite one_sprite;
	public Sprite two_sprite;
	public Sprite three_sprite;
	public Sprite four_sprite;
	public Sprite five_sprite;
	public Sprite six_sprite;
	public Sprite seven_sprite;
	public Sprite eight_sprite;
	public Sprite nine_sprite;
	public Sprite zero_sprite;
	*/

	public Sprite[] numeral_sprite = new Sprite[10];
	public Sprite crown_sprite;

	// Use this for initialization
	void Awake () {
		//GetComponent<SpriteRenderer>().enabled = false;

	}
	
	// Update is called once per frame
	//void Update () {
		
	//}

	public void hide() {
		
		GetComponent<SpriteRenderer>().enabled = false;

	}
	// 
	public void set_x(float new_x) {
		transform.localPosition = new Vector3 (new_x,0,0);
			//.Set ((float)new_x,0f,0f);
		//x = new_x;
	}

	public void set_num(int new_num) {
		// this is being called before Start


		//GetComponent<SpriteRenderer> ().sprite = "1.png";

		//GetComponent<SpriteRenderer> ().sprite = Resources.Load("1.png", typeof(Sprite)) as Sprite;

		if (new_num > 9)
			return;


		//gameObject.SpriteRenderer.enabled = true;

		GetComponent<SpriteRenderer>().sprite = numeral_sprite [new_num];
		GetComponent<SpriteRenderer>().enabled = true;




		//Debug.Log ("numeral_script > set_num" + new_num);
	}


}
