﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class join_sprite_script : MonoBehaviour {

	// blue joins
	public Sprite join_U;

	public Sprite join_UR;
	public Sprite join_UD;

	public Sprite join_URD;

	public Sprite join_URDL;

	// pink sharebubbles / sharepipes
	public Sprite share_bubble_UD;
	public Sprite share_bubble_DR;

	public Sprite share_bubble_RDL;

	public Sprite share_bubble_UDLR;

	public Sprite share_pipe_U;

	public Sprite share_pipe_UR;
	public Sprite share_pipe_UD;

	public Sprite share_pipe_URD;

	public Sprite share_pipe_URDL;


	public bool join_up;
	public bool join_left;
	public bool join_down;
	public bool join_right;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	//void Update () {
		
	//}

	public void reset () {
		join_up = false;
		join_down = false;
		join_left = false;
		join_right = false;
		hide ();
		transform.localRotation = Quaternion.AngleAxis (0, Vector3.forward);
		transform.localPosition = new Vector2 (0f, 0f);
	}

	public void calc_sharesquare() {
		int num_joins = 0;
		if (join_up == true)
			num_joins++;
		if (join_down == true)
			num_joins++;
		if (join_left == true)
			num_joins++;
		if (join_right == true)
			num_joins++;

		Quaternion rotation = Quaternion.AngleAxis (0, Vector3.forward);



		if (num_joins == 2) {



			if ((join_left && join_right) || (join_up && join_down))
				GetComponent<SpriteRenderer> ().sprite = share_bubble_UD;	// opposite
			else
				GetComponent<SpriteRenderer> ().sprite = share_bubble_DR; 	// elbow

			// opposite sides
			if (join_left && join_right) rotation = Quaternion.AngleAxis (90, Vector3.forward);

			// elbow
			if (join_right && join_down) rotation = Quaternion.AngleAxis (90, Vector3.forward);
			if (join_down && join_left) rotation = Quaternion.AngleAxis (180, Vector3.forward);
			if (join_left && join_up) rotation = Quaternion.AngleAxis (-90, Vector3.forward);

			transform.localRotation = rotation;
		} else if (num_joins == 3) {

			if (join_right == false) rotation = Quaternion.AngleAxis (90, Vector3.forward);
			if (join_up == false) rotation = Quaternion.AngleAxis (0, Vector3.forward);
			if (join_down == false) rotation = Quaternion.AngleAxis (-90, Vector3.forward);

			GetComponent<SpriteRenderer> ().sprite = share_bubble_RDL;
			transform.localRotation = rotation;
		} else if (num_joins == 4) {
			GetComponent<SpriteRenderer> ().sprite = share_bubble_UDLR;
		}
	}

	public void calc_sharepipe() {
		int num_joins = 0;
		if (join_up == true)
			num_joins++;
		if (join_down == true)
			num_joins++;
		if (join_left == true)
			num_joins++;
		if (join_right == true)
			num_joins++;

		Quaternion rotation = Quaternion.AngleAxis (0, Vector3.forward);

		if (num_joins == 2) {



			if ((join_left && join_right) || (join_up && join_down))
				GetComponent<SpriteRenderer> ().sprite = share_pipe_UD;	// opposite
			else
				GetComponent<SpriteRenderer> ().sprite = share_pipe_UR; 	// elbow

			// opposite sides
			if (join_left && join_right) rotation = Quaternion.AngleAxis (90, Vector3.forward);

			// elbow
			if (join_right && join_down) rotation = Quaternion.AngleAxis (0, Vector3.forward);
			else if (join_down && join_left) rotation = Quaternion.AngleAxis (90, Vector3.forward);
			else if (join_left && join_up) rotation = Quaternion.AngleAxis (180, Vector3.forward);
			else if (join_right && join_up) rotation = Quaternion.AngleAxis (-90, Vector3.forward);

			transform.localRotation = rotation;
		} else if (num_joins == 3) {

			if (join_right == false) rotation = Quaternion.AngleAxis (90, Vector3.forward);
			if (join_up == false) rotation = Quaternion.AngleAxis (0, Vector3.forward);
			if (join_down == false) rotation = Quaternion.AngleAxis (-90, Vector3.forward);

			GetComponent<SpriteRenderer> ().sprite = share_pipe_URD;
			transform.localRotation = rotation;
		} else if (num_joins == 4) {
			GetComponent<SpriteRenderer> ().sprite = share_pipe_URDL;
		}

		//GetComponent<SpriteRenderer> ().sprite = share_pipe_UD;
	}

	public void calc_join_sprite() {

		int num_joins = 0;
		if (join_up == true)
			num_joins++;
		if (join_down == true)
			num_joins++;
		if (join_left == true)
			num_joins++;
		if (join_right == true)
			num_joins++;

		if (num_joins == 1) {
			Quaternion rotation = Quaternion.AngleAxis (180, Vector3.forward);
			if (join_up == true) {
			} else if (join_down == true) {
				rotation = Quaternion.AngleAxis (0, Vector3.forward);
			} else if (join_left == true) {
				rotation = Quaternion.AngleAxis (90, Vector3.forward);
			} else if (join_right == true) {
				rotation = Quaternion.AngleAxis (-90, Vector3.forward);
			}

			GetComponent<SpriteRenderer> ().sprite = join_U;
			transform.localRotation = rotation;

		}

	}

	public void make_vis() {
		GetComponent<SpriteRenderer> ().enabled = true;
	}

	public void hide() {
		GetComponent<SpriteRenderer> ().enabled = false;
	}
}
