﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class world_button_script : MonoBehaviour {

	public int buttonnum = 0;
	public world_list theparent;

	// Use this for initialization
	void Start () {
		theparent = GetComponentInParent<world_list> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnMouseDown () {
		theparent.on_button_click (buttonnum);
	}
}
