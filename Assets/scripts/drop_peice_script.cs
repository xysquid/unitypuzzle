﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class drop_peice_script : MonoBehaviour {

	private int [,] tiles = new int[5,5];
	private GameObject[,] block_objs = new GameObject[5, 5];

	public GameObject droptile_prefab;
	public float tile_size;

	public GameObject game_grid;

	public int drop_peice_state = 0;
	// NO_STATE: 0,
	// IN_GRID: 1,
	// OUT_GRID: 2,
	// PLAYER: 3,
	// NOT_IN_LEVEL: 4,

	public Sprite red_tile_sprite;
	public Sprite yellow_tile_sprite;

	// Use this for initialization
	void Start () {
		for (int x = 0; x < 5; x++) {
			for (int y = 0; y < 5; y++) {
				block_objs [x, y] = Instantiate (droptile_prefab, new Vector2 (x * tile_size, y * tile_size), Quaternion.identity) as GameObject;
				block_objs[x,y].transform.SetParent (transform, false);
				//droptile_script d_script = block_objs[x,y].GetComponent<droptile_script>();
				SpriteRenderer dImg = block_objs[x,y].GetComponent<SpriteRenderer>();
				dImg.sprite = red_tile_sprite;

				tiles [x, y] = 0;
			}
		}

		tiles [1, 0] = 1;
		tiles [2, 0] = 1;
		tiles [2, 1] = 1;
		tiles [3, 0] = 1;

		for (int x = 0; x < 5; x++) {
			for (int y = 0; y < 5; y++) {
				if (tiles [x, y] == 0) {
					SpriteRenderer dImg = block_objs[x,y].GetComponent<SpriteRenderer>();
					dImg.enabled = false;
				}
			}
		}
	}

	private bool drag = false;
	private Vector3 drag_offset = new Vector3();

	public void OnMouseDown () {
		//Debug.Log ("mousedown on drop peice");

		drag_offset = transform.position - Camera.main.ScreenToWorldPoint(Input.mousePosition);
		// check if we are over an actual nonzero tile
		// drag_offset -> local x, y

		int x = Mathf.FloorToInt(Mathf.Floor (-1 * drag_offset.x / tile_size));
		int y = Mathf.FloorToInt(Mathf.Floor (-1 * drag_offset.y / tile_size));
		//Debug.Log ("x " + x + " y " + y);

		if (x < 0 || y < 0 || x > 4 || y > 4)
			return;
		
		if (tiles [x, y] == 0)
			return;

		drag = true;
		drop_peice_state = 3;	// player
		make_vis();
	}

	private bool drop_into_grid (int tilex, int tiley) {

		// if check match fails return false
		// checks for walls, lockout, other peices

		for (int x = 0; x < 5; x++) {
			for (int y = 0; y < 5; y++) {
				if (tiles [x, y] == 0)
					continue;
				if(tilex + x >= (int)game_grid.GetComponent<game_grid>().get_grid_w() || 
					tiley + y >= (int)game_grid.GetComponent<game_grid>().get_grid_h())	return false;	// out of bounds

				// game grid x y -> set to my colour

				// game grid x y -> set to 'mine' and my id
				game_grid.GetComponent<game_grid>().change_tile(tilex + x, tiley + y, 2);

				// game grid.fill_lockout_group(x, y);		and we do nothing with any returned info

			} // for y
		} // for x

		// this.update pos to on the grid

		// 
		drop_peice_state = 1;	// IN_GRID

		// hide
		hide();

		return true;
	}

	public void hide () {
		for (int x = 0; x < 5; x++) {
			for (int y = 0; y < 5; y++) {
				SpriteRenderer dImg = block_objs[x,y].GetComponent<SpriteRenderer>();
				dImg.enabled = false;
			}
		}
		
	}

	public void make_vis () {
		for (int x = 0; x < 5; x++) {
			for (int y = 0; y < 5; y++) {
				SpriteRenderer dImg = block_objs[x,y].GetComponent<SpriteRenderer>();
				if (tiles [x, y] == 0) {
					dImg.enabled = false;
				} else
					dImg.enabled = true;
			}
		}
	}

	public void OnMouseUp () {
		//Debug.Log ("mouseup on drop peice");
		drag = false;
		drop_into_grid (hovertilex , hovertiley);
		Debug.Log ("hovertilex " + hovertilex + " hovertiley " + hovertiley);
	}

	int hovertilex = 0;
	int hovertiley = 0;

	public void OnMouseDrag () {
		// https://docs.unity3d.com/ScriptReference/Camera.ScreenToWorldPoint.html
		if (drag == false) return;

		//Debug.Log ("mousemove on drop peice");


		// could also Math.round() these x y values:
		Vector2 pscreen = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);

		//Vector3 pworld = Camera.main.ScreenToWorldPoint (pscreen);	// have to tag a camera as 'main'

		Vector3 pworld = Camera.main.ScreenToWorldPoint (Input.mousePosition);	// have to tag a camera as 'main'
		pworld.z = 0;
		//transform.Translate (pworld);
		transform.position = pworld + drag_offset;

		hovertilex = Mathf.FloorToInt (pworld.x/tile_size);
		hovertiley = Mathf.FloorToInt (pworld.y/tile_size);

		//Debug.Log ("screen x : " + Input.mousePosition.x + "-- screen y : " + Input.mousePosition.y + 
		//	" -- world x : " + pworld.x + " world y : " + pworld.y);

		//Vector3 pz = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		//pz.z = 0;
		//transform.Translate (pz);

		return;

		//transform.Translate (Camera.main.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, c.nearClipPlane)));

		//transform.Translate (Camera.main.ScreenPointToRay(Input.mousePosition));	// Input.mousePosition is 3D ?

		//Debug.Log ("Ecent.current " + Event.current);


		Vector3 p = new Vector3 ();
		Camera c = Camera.main;

		Event e = Event.current;

		Vector2 mousePos = new Vector2 ();
		//Debug.Log (e.mousePosition.x);

		//mousePos.x = e.mousePosition.x;	// !!! cant use outside onGUI
		return;
		//mousePos.y = c.pixelHeight - e.mousePosition.y;

		//p = c.ScreenToWorldPoint (new Vector3(mousePos.x, mousePos.y, c.nearClipPlane ));

		//Debug.Log (p.x);

		//Debug.Log (Camera.main.ScreenToWorldPoint (mousePos));

		//Vector3 pz = Camera.main.ScreenToWorldPoint (mousePos);
	
		//pz.Set
		//pz = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		//pz.z = 0;
		//transform.position = pz;
		//transform.Translate(Camera.main.ScreenToWorldPoint(Input.mousePosition));
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
