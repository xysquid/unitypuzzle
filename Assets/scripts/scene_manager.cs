﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AppTrackerUnitySDK;

public class scene_manager : MonoBehaviour {

	// this is like change_state in my HTML5 build
	// and maybe also like play_state.current_level,

	// ALL changes between Unity scenes will be wrapped here

	public static scene_manager scene_manager_instance;

	public int current_level = 0;

	public int current_world = 0;

	// Use this for initialization
	void Awake () {
		// No way this should be called twice if it only lives in BootUp Scene, but...
		if (scene_manager_instance == null) {
			scene_manager_instance = this;
			DontDestroyOnLoad (transform.root.gameObject);
		} else {
			DestroyObject(gameObject);
		}

		// ALSO: maybe should spawn the game_grid_instance
		//		 maybe it can live in DuringGame because DuringGame is only ever called from this file
		//		 so I can always instantiate it first 

		Application.LoadLevel ("DuringGame");	// just to instantiate the game_grid_instance
												// this seems dodgy


		Application.targetFrameRate = 60;	// default is 30 on mobile
											// https://divillysausages.com/2016/01/21/performance-tips-for-unity-2d-mobile/


		if (Application.platform == RuntimePlatform.Android) {

			// http://help.leadbolt.com/using-unity-android/

			// Initialize Leadbolt SDK with your API Key
			// Mine of Sight Android: eCIoUUv9aO2PxseHdnpIRa5NTQF5WuhC
			AppTrackerAndroid.startSession("eCIoUUv9aO2PxseHdnpIRa5NTQF5WuhC", true);

			// Do I need to:
			// AndroidJavaClass plugin = new AndroidJavaClass("fullname");
			// http://answers.unity3d.com/questions/849692/leadbolt-unity-plugin-example-gets-jni-exception.html

			// cache a non rewareded ad
			AppTrackerAndroid.loadModuleToCache("inapp");
		}

	}

	void Start () {
		
		// load stuff

		// go to the game ... Menu, Levels, DuringGame

		// never return to this Unity Scene

		Application.LoadLevel ("Levels");
		game_grid.game_grid_instance.gameObject.SetActive(false);


		ui_script.ui_instance.on_overworld_start ();

	}

	// Update is called once per frame
	void Update () {
		
	}

	// called by GetReward scene
	public void start_level() {
		start_level (current_level, current_world);
	}

	public void start_next_level() {
		current_level++;
		start_level (current_level, current_world);
	}

	public void retry_level() {
		start_level (current_level, current_world);
	}

	public void goto_random() {
		// this mode will be gone later on...
		level_grid.level_grid_instance.gameObject.SetActive(false);
		Application.LoadLevel ("DuringGame");	// game_grid.game_grid_instance now exists if it didn't already

		//game_grid.game_grid_instance.set_world (current_world);

		game_grid.game_grid_instance.generate_rand();
		game_grid.game_grid_instance.gameObject.SetActive(true);
		game_grid.game_grid_instance.start_level ();


	}



	// called by game_grid.check_for_victory
	public void player_is_almost_finished_level() {

		if (levels_until_nonrewarded_ad != 1)
			return;

		levels_until_nonrewarded_ad = 0;

		if (Application.platform == RuntimePlatform.Android) {
			// cache a non rewareded ad
			AppTrackerAndroid.loadModuleToCache("inapp");
		}
	}

	// needs to be saved between sessions
	public int levels_until_rewarded_ad = 3;
	public int levels_until_nonrewarded_ad = 3;
	public int levels_until_beg = 50;

	public void start_level (int lvl, int world = 0) {

		level_grid.level_grid_instance.gameObject.SetActive(false);

		Debug.Log ("scene_man > start_level " + lvl);

		current_level = lvl;
		current_world = world;


		menu_script.g_current_level = current_level;

		// controls application flow
		// do we need to show ad / tutorial / begforrating / rewardedvideo / etc...
		// and then return to this function?


		levels_until_rewarded_ad--;
		levels_until_nonrewarded_ad--;
		if (levels_until_beg > - 10) levels_until_beg--;

		if (levels_until_beg == 0) {
			// dont reset - show 1x
			// BegForRating, upon ending, calls start_level

			// return
		}

		if (levels_until_rewarded_ad <= 0) {
			levels_until_rewarded_ad = (int)Random.Range(5,10);

			if (levels_until_nonrewarded_ad == 0)
				levels_until_nonrewarded_ad = 1;

			// rewardedvideo state or interstitial
			// which will call start_level again
			game_grid.game_grid_instance.gameObject.SetActive(false);
			Application.LoadLevel ("GetReward");
			return;
		}

		if (levels_until_nonrewarded_ad <= 0) {
			levels_until_nonrewarded_ad = (int)Random.Range(5,10);

			if (Application.platform == RuntimePlatform.Android) {
				if (AppTrackerAndroid.isAdReady("inapp") == true) AppTrackerAndroid.loadModule("inapp");

				// Where shall I cache a new ad?
				// Should be during a level when the player is ~75% 
				// see player_is_almost_finished_level() above
			}

		}

		//else if (tutorial) {
		//		show after any ads
		// 		goto tutorial state
		// 		return
		//}


		Application.LoadLevel ("DuringGame");	// game_grid.game_grid_instance now exists if it didn't already

		//game_grid.game_grid_instance.set_world (current_world);
		game_grid.game_grid_instance.load_level (lvl, current_world);
		game_grid.game_grid_instance.gameObject.SetActive(true);
		Debug.Log ("scene_man > start_level ... calling game_grid.game_grid_instance.start_leve ");
		game_grid.game_grid_instance.start_level ();


		if (game_grid.game_grid_instance.text_a_string != "")
			ui_script.ui_instance.set_ingame_text (game_grid.game_grid_instance.text_a_string, game_grid.game_grid_instance.text_b_string);
		else
			ui_script.ui_instance.hide_ingame_text ();


		
		

	}

	public void goto_world_list () {
		ui_script.ui_instance.on_overworld_start ();
		game_grid.game_grid_instance.gameObject.SetActive(false);
		Application.LoadLevel ("Worlds");	

		level_grid.level_grid_instance.gameObject.SetActive(false);

	}

	public void goto_overworld (int world_) {
		current_world = world_;
		goto_overworld ();

	}

	public void goto_overworld () {
		

		game_grid.game_grid_instance.gameObject.SetActive(false);
		Application.LoadLevel ("Levels");	
		level_grid.level_grid_instance.load_page ();
		level_grid.level_grid_instance.gameObject.SetActive(true);
		ui_script.ui_instance.on_overworld_start ();
	
	}

	public void goto_menu () {

	}
}
