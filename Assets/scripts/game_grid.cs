﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class game_grid : MonoBehaviour {

	public static int grid_w = 10;
	public static int grid_h = 10;
	public float tile_size = 60;
	public GameObject block_prefab;

	private GameObject[,] blocks = new GameObject[grid_w, grid_h];
	private block_script[,] block_scripts = new block_script[grid_w, grid_h];

	public GameObject cursor;
	private int cursor_x = -1;
	private int cursor_y = -1;

	public int[,] joined_tiles = new int [grid_w, grid_h];

	public static game_grid game_grid_instance;

	public int dig_flag_selector = 0;

	public int num_joined_groups = 0;

	private enum InGameState {NoState, SwoopIn, Playing, Win, Lose, SwoopOut};
	// game_grid.InGameState.Win

	private game_grid.InGameState current_state;

	private int screen_width = 0;
	private int screen_height = 0;

	void Awake () {

		// this would be called at the start of every level

		if (game_grid_instance == null) {
			game_grid_instance = this;
			DontDestroyOnLoad (transform.root.gameObject);
			setup_grid ();
		} else {
			DestroyObject(gameObject);

		}

		screen_width = Screen.width;
		screen_height = Screen.height;


		// All Awakes run before any Starts run before any Updates. 



	}

	private bool seq_lengths_done_this_level = false;

	public void calc_seq_lengths() {
		if (seq_lengths_done_this_level == true)
			return;
		seq_lengths_done_this_level = true;

		for (int x = 0; x < grid_w; x++) {
			for (int y = 0; y < grid_h; y++) {
				block_scripts [x, y].my_vert_seq_length = 0;
				block_scripts [x, y].my_horiz_seq_length = 0;
				// my vert/horiz seq id ? do I need?
				block_scripts [x, y].my_vert_seq_id = -1;
				block_scripts [x, y].my_horiz_seq_id = -1;
			}


		}

		// vert
		int current_seq = 0;
		int current_seq_tiles = 0;
		int current_vert_group_id = 0;
		int current_horiz_group_id = 0;

		for (int x = 0; x < grid_w; x++) {
			current_seq = 0;
			current_seq_tiles = 0;
			for (int y = 0; y < grid_h; y++) {
				int b_type = get_block_type (x, y);

				if (b_type == 2 && current_seq == 0) current_vert_group_id++;

				if (b_type == 2)
					current_seq += get_num_mines (x, y);
				else
					current_seq = 0;

				if (b_type == 2)
					current_seq_tiles++;
				else
					current_seq_tiles = 0;

				for (var mine_y = y - current_seq_tiles + 1; mine_y <= y; mine_y++) {
					if (mine_y >= grid_h || mine_y < 0)
						continue;
					block_scripts[x,mine_y].my_vert_seq_length = current_seq;
					block_scripts[x,mine_y].my_vert_seq_id = current_vert_group_id;
				}
			}
		}

		// horiz
		current_seq = 0;
		current_seq_tiles = 0;
		for (int y = 0; y < grid_h; y++) {
			current_seq = 0;
			current_seq_tiles = 0;
			for (int x = 0; x < grid_w; x++) {
				int b_type = get_block_type (x, y);

				if (b_type == 2 && current_seq == 0) current_horiz_group_id++;

				if (b_type == 2)
					current_seq += get_num_mines (x, y);
				else
					current_seq = 0;

				if (b_type == 2)
					current_seq_tiles++;
				else
					current_seq_tiles = 0;

				for (var mine_x = x - current_seq_tiles + 1; mine_x <= x; mine_x++) {
					if (mine_x >= grid_w || mine_x < 0)
						continue;
					block_scripts[mine_x,y].my_horiz_seq_length = current_seq;
					block_scripts[mine_x,y].my_horiz_seq_id = current_horiz_group_id;
				}
			}
		}


	}

	private void set_cursor(int x, int y) {
		cursor_x = x;
		cursor_y = y;

		if (is_in_grid (x, y) == false) {
			cursor.SetActive (false);	
		} else {
			cursor.SetActive (true);
			cursor.gameObject.transform.localPosition = new Vector2 (x*tile_size, y*tile_size);
		}
	}

	public bool is_in_grid(int x, int y) {
		if (x < 0 || y < 0 || x >= grid_w || y >= grid_w)
			return false;
		return true;
	}

	public int get_horiz_seq_length(int x, int y) {
		calc_seq_lengths ();

		if (is_in_grid (x, y) == false)
			return 0;

		return block_scripts [x, y].my_horiz_seq_length;

	}

	public int get_vert_seq_length(int x, int y) {
		calc_seq_lengths ();

		if (is_in_grid (x, y) == false)
			return 0;
		
		return block_scripts [x, y].my_vert_seq_length;

		return 0;
	}

	public int get_block_x_by_index(int index) {
		int x = index / grid_w; //
		return x;
	}

	public int get_block_y_by_index(int index) {
		int y = index % grid_w;
		return y;
	}

	public int get_block_type_by_index(int index) {
		int y = index % grid_w;
		int x = index / grid_w; // rounds to floor anyway according to someone on stackoverflow //Math.Floor (index / grid_w);

		if (x < 0 || y < 0 || x > grid_w - 1 || y > grid_h - 1) {
			Debug.Log ("get_block x y out of range!!!");
			x = 0;
			y = 0;
		}

		return get_block_type (x, y);
	}

	public int get_block_type(int x, int y) {
		if (x < 0 || y < 0 || x > grid_w - 1 || y > grid_h - 1) {
			Debug.Log ("get_block x y out of range!!!");
			x = 0;
			y = 0;
		}

		return blocks [x, y].GetComponent<block_script> ().block_type;
	}

	public int get_index_of_block(int x, int y) {

		if (x < 0 || y < 0 || x > grid_w - 1 || y > grid_h - 1) {
			Debug.Log ("get_block x y out of range!!!");
			x = 0;
			y = 0;
		}

		int theindex = y + x * grid_w;
		return theindex;
	}

	public block_script get_block_by_index(int index) {



		int y = index % grid_w;
		int x = index / grid_w; // rounds to floor anyway according to someone on stackoverflow //Math.Floor (index / grid_w);

		if (x < 0 || y < 0 || x > grid_w - 1 || y > grid_h - 1) {
			Debug.Log ("get_block x y out of range!!!");
			x = 0;
			y = 0;
		}
			
		if (index != blocks [x, y].GetComponent<block_script> ().index) {
			Debug.LogError ("get_block_by_index > wrong");
		}

		return blocks [x,y].GetComponent<block_script>();
	}

	public block_script get_block(int x, int y) {

		if (x < 0 || y < 0 || x > grid_w - 1 || y > grid_h - 1) {
			Debug.Log ("get_block x y out of range!!!");
			x = 0;
			y = 0;
		}

		return blocks [x,y].GetComponent<block_script>();;
	}

	private void setup_grid() {
		int block_index = 0;
		for (int x = 0; x < grid_w; x++) {
			for (int y = 0; y < grid_h; y++) {
				blocks [x, y] = Instantiate (block_prefab, new Vector2 (x * tile_size, y * tile_size), Quaternion.identity) as GameObject;
				//GameObject new_block = Instantiate (block_prefab, new Vector2 (x * tile_size, y * tile_size), Quaternion.identity) as GameObject;
				//new_block.transform.position = (new Vector2 (x * tile_size, y * tile_size));
				//new_block.block_script.game_state = transform;
				//blocks[x][y] = new_block;
				blocks[x,y].transform.SetParent (transform, false);
				block_script b_script = blocks [x, y].GetComponent<block_script>();

				block_scripts [x, y] = b_script;

				b_script.x = x;
				b_script.y = y;
				//b_script.set_type (2);
				b_script.game_state = gameObject;

				b_script.index = block_index;
				block_index++;

				//blocks [x, y].transform.GetComponent<block_script>.y = y;
				//blocks [x, y].GetComponent<block_script>.set_type (1);

			}
		}

	}

	public int get_join_group(int x, int y) {
		if (x < 0 || y < 0 || x > grid_w - 1 || y > grid_h - 1)
			return -1;
		return joined_tiles [x, y];
	}

	public void join_tiles(int x, int y, int xx, int yy) {

		block_script block_a = get_block (x, y);
		block_script block_b = get_block (xx, yy);

		block_script.HintType hint_ = block_b.hint_type;

		if (hint_ == block_script.HintType.Compass ||
		    hint_ == block_script.HintType.EyeBracket)
			return;

		int join_group = -1;

		if (block_a.join_group != 0 ||
		    block_b.join_group != 0) {
			// FOR NOW both must be unjoined
			// I'll allow extended joins later
			// see the code in MoS HTML5
			return;
		}

		if (joined_tiles [x, y] == 0 &&
			joined_tiles [xx, yy] == 0) {

			num_joined_groups++;

			block_a.join_group = num_joined_groups;
			block_b.join_group = num_joined_groups;

			joined_tiles [x, y] = num_joined_groups;
			joined_tiles [xx, yy] = num_joined_groups;

			block_a.join_second = true;
			block_b.join_leader = true;

			block_b.sec_join_leader_index = block_a.index;

			block_a.my_join_leader_x = xx;
			block_a.my_join_leader_y = yy;
			block_a.i_know_my_join_leader = true;

			block_b.my_join_leader_x = xx;
			block_b.my_join_leader_y = yy;
			block_b.i_know_my_join_leader = true;

			join_group = num_joined_groups;
		}

		block_a.hint_type = hint_;
		block_b.hint_type = hint_;

		block_a.calc_joiner_sprite ();
		block_b.calc_joiner_sprite ();
	}


	public void uncover_joined_group(int joingroup) {
		for (int x = 0; x < grid_w; x++) {
			for (int y = 0; y < grid_h; y++) {
				block_script block_ = get_block (x, y);
				if (block_.join_group == joingroup){
					block_.uncover_joined();
				}
			}
		}
	}

	// Use this for initialization
	void Start () {
		


		//load_level (2);
	}

	private bool victory = false;


	private void check_for_victory() {

		victory = true;

		int unsolved = 0;
		int mistakes_ = 0;

		for (int x = 0; x < grid_w; x++) {
			for (int y = 0; y < grid_h; y++){

				if (block_scripts [x, y].covered_up == true && 
					block_scripts [x, y].flag_on == true &&
				    block_scripts [x, y].block_type == 0)
					mistakes_++;

				if (block_scripts [x, y].covered_up == true && block_scripts [x, y].flag_on == false) {
					victory = false;
					unsolved++;
				}

				if (block_scripts [x, y].block_type == 2 && block_scripts [x, y].flag_on == false)
					victory = false;

				if (block_scripts [x, y].block_type == 0 && block_scripts [x, y].covered_up == true)
					victory = false;

				// In some cases we need to know how close the player is to winning
				// Mostly not though, so we can end this scan early
				if (scene_manager.scene_manager_instance.levels_until_nonrewarded_ad != 1 &&
				    victory == false)
					return;
			}
		}

		if (mistakes_ == 0 &&
		    unsolved < 20) {
			scene_manager.scene_manager_instance.player_is_almost_finished_level ();
		}

		if (victory == false)
			return;

		// victory!
		victory = true;
		ui_script.ui_instance.on_ingame_end();
		ui_script.ui_instance.on_win_start ();
		current_state = game_grid.InGameState.Win;

		if (scene_manager.scene_manager_instance.current_world == 0) {
			PlayerPrefs.SetInt ("mainlevel" + scene_manager.scene_manager_instance.current_level, 1);
				
		}
	}

	public void select_dig() {
		dig_flag_selector = 1;
		Debug.Log(" dig_flag_selector " + dig_flag_selector);

		if (is_in_grid (cursor_x, cursor_y) == true) {
			this.dig_tile (cursor_x, cursor_y);
			check_for_victory ();
		}
	}

	public void select_flag() {
		dig_flag_selector = 2;
		Debug.Log(" dig_flag_selector " + dig_flag_selector);

		if (is_in_grid (cursor_x, cursor_y) == true) {
			this.flag_tile (cursor_x, cursor_y);
			check_for_victory ();
		}
	}

	public void move_cursor (int x_dir, int y_dir) {
		if (x_dir < 0 && cursor_x <= 0)
			cursor_x = grid_w - 1;
		else if (y_dir < 0 && cursor_y <= 0)
			cursor_y = grid_h - 1;
		else if (x_dir > 0 && cursor_x == grid_w - 1)
			cursor_x = 0;
		else if (y_dir > 0 && cursor_y == grid_h - 1)
			cursor_y = 0;
		else {
			cursor_x += x_dir;
			cursor_y += y_dir;
		}

		Debug.Log ("move cursor " + x_dir + " " + y_dir);

		if (cursor_x <= -1)
			cursor_x = 0;
		if (cursor_y <= -1)
			cursor_y = 0;



		set_cursor (cursor_x, cursor_y);
		
	}

	public void tile_clicked (int x, int y) {
		Debug.Log ("ticle clicked x:" + x + " y:" + y + " dig_flag_selector " + dig_flag_selector);

		// need to check the state of the action_button_container
		// are we in dig / flag mode ... 'maybe' mode

		// this is for when the player taps directly on the grid
		// another type of control is with the gamepad

		if (block_scripts [x, y].covered_up == false &&
		    block_scripts [x, y].hint_type != block_script.HintType.NoHint) {
			ui_script.ui_instance.describe_hint (block_scripts [x, y].hint_type);
		}



		if (block_scripts [x, y].covered_up == true) {
			set_cursor (x, y);
		} else {
			set_cursor (-1, -1);
		}


		/*

		if (dig_flag_selector == 1)
			this.dig_tile (x, y);
		else
			this.flag_tile (x, y);
		

		check_for_victory ();
		*/
	}

	public void flag_tile (int x, int y) {
		//block_script b_script = blocks [x, y].GetComponent<block_script> ();
		block_scripts[x,y].toggle_flag();
	}

	public void dig_tile (int x, int y) {
		//block_script b_script = blocks [x, y].GetComponent<block_script> ();
		block_scripts[x,y].uncover();

		if (block_scripts [x, y].block_type == 2) {
			// Hit a mine!
			current_state = InGameState.Lose;
			ui_script.ui_instance.on_ingame_end();
			ui_script.ui_instance.on_lose_start ();

			victory = false;

			// Effects
			StartCoroutine("do_gameover_effect");
		}
	}

	public void change_tile (int x, int y, int type) {
		if (x < 0 || y < 0 || x > grid_w - 1 || y > grid_h - 1)
			return;
		block_script b_script = blocks [x, y].GetComponent<block_script>();
		b_script.set_type (type);
		Debug.Log ("change_tile x " + x + " y " + y);
	}

	public bool is_mine_lonely(int x, int y) {
		if (x < 0 || y < 0 || x > grid_w - 1 || y > grid_h - 1)
			return false;

		if (get_num_mines(x,y) == 0) return false;

		if (x > 0 && get_num_mines(x - 1, y) > 0) return false;
		if (y > 0 && get_num_mines(x, y - 1) > 0) return false;
		if (x < grid_w - 1 && get_num_mines(x + 1, y) > 0) return false;
		if (y < grid_h - 1 && get_num_mines(x, y + 1) > 0) return false;

		return true;
	}

	public bool is_flag_lonely(int x, int y) {
		// not bool - needs to return YES NO UNSURE
		// see MoS HTML5 blockclass.is_flag_lonely

		if (x < 0 || y < 0 || x > grid_w - 1 || y > grid_h - 1)
			return false;

		if (get_num_player_flags(x,y) == 0) return false;

		if (x > 0 && get_num_player_flags(x - 1, y) > 0) return false;
		if (y > 0 && get_num_player_flags(x, y - 1) > 0) return false;
		if (x < grid_w - 1 && get_num_player_flags(x + 1, y) > 0) return false;
		if (y < grid_h - 1 && get_num_player_flags(x, y + 1) > 0) return false;

		return true;
	}

	public int get_num_player_flags (int x, int y) {
		if (x < 0 || y < 0 || x > grid_w - 1 || y > grid_h - 1)
			return 0;

		block_script b_script = blocks [x, y].GetComponent<block_script>();
		int nummines = b_script.get_num_mines ();

		if (b_script.flag_on == false)
			return 0;

		return nummines;
	}

	public int get_num_mines (int x, int y) {
		if (x < 0 || y < 0 || x > grid_w - 1 || y > grid_h - 1)
			return 0;


		block_script b_script = blocks [x, y].GetComponent<block_script>();
		int nummines = b_script.get_num_mines ();
		return nummines;
	}

	public int get_grid_w () {
		return grid_w;
	}

	public int get_grid_h () {
		return grid_h;
	}

	public void reset_join_tiles () {
		for (int x = 0; x < grid_w; x++) {
			for (int y = 0; y < grid_h; y++) {
				joined_tiles [x, y] = 0;
			}
		}

	}


	IEnumerator do_explosion_effect () {
		yield return null;
	}

	IEnumerator do_gameover_effect () {
		float lose_y_vel = 0.1f;
		float lose_y_pos = 0f;

		for (int i = 0; i < 1000; i++) {

			lose_y_pos += lose_y_vel;
			lose_y_vel -= 0.2f * lose_y_pos + 0.05f;

			transform.position = new Vector3 (transform.position.x, lose_y_pos, transform.position.z);

			if (current_state == InGameState.Lose)
				yield return null;
			else {
				break;
				yield return null;
			}
		}
		yield return null;
			//transform.position = new Vector3 (0, transform.position.y, transform.position.z);

	}

	IEnumerator swoop_in () {


		
		float resting_x = 0f;//transform.position.x;
		float distance_x = 20f;	// 20 to the right
		float swoop_time = 1;	// 1 seconds
		
		float current_x = resting_x + distance_x; 

		float start_time = Time.time; // seconds since 1970

		for (float time_passed = 0f; time_passed < swoop_time; time_passed = Time.time - start_time) {
		//for (float x = 20; x > resting_x; x -= 0.5f){ //0.01f + 0.005f*x*x) {

			// time_passed = 0 -> current_x = distance_x + resting_x
			// time_passed = swoop_time -> current_x = resting_x
			current_x = distance_x + resting_x - (time_passed / swoop_time)  * distance_x;

			//current_x = distance_x + resting_x - (time_passed / swoop_time) *(time_passed / swoop_time) * distance_x;

			transform.position = new Vector3 ((float)current_x, transform.position.y, transform.position.z);

			yield return null;
		}

		transform.position = new Vector3 (resting_x, transform.position.y, transform.position.z);

		//transform.position = new Vector3 (+1000, 0, 0);
		current_state = game_grid.InGameState.Playing;
		ui_script.ui_instance.on_ingame_start ();
	}



	IEnumerator swoop_out () {

		float old_x = transform.position.x;

		float resting_x = -20;
		float distance_x = 20f;	// 20 to the right
		float swoop_time = 1;	// 1 seconds

		float current_x = resting_x + distance_x; 

		float start_time = Time.time; // seconds since 1970

		for (float time_passed = 0f; time_passed < swoop_time; time_passed = Time.time - start_time) {
			//for (float x = 20; x > resting_x; x -= 0.5f){ //0.01f + 0.005f*x*x) {

			// time_passed = 0 -> current_x = distance_x + resting_x
			// time_passed = swoop_time -> current_x = resting_x
			current_x = distance_x + resting_x - (time_passed / swoop_time)  * distance_x;

			//current_x = distance_x + resting_x - (time_passed / swoop_time) *(time_passed / swoop_time) * distance_x;

			transform.position = new Vector3 ((float)current_x, transform.position.y, transform.position.z);

			yield return null;
		}

		transform.position = new Vector3 (old_x, transform.position.y, transform.position.z);

		//transform.position = new Vector3 (+1000, 0, 0);
		//current_state = game_grid.InGameState.Playing;
		//ui_script.ui_instance.on_ingame_start ();

		if (victory == true)
			scene_manager.scene_manager_instance.start_next_level();
		else
			scene_manager.scene_manager_instance.retry_level();
	}

	public void retry_level() {
		if (current_state == game_grid.InGameState.SwoopOut)
			return;

		current_state = game_grid.InGameState.SwoopOut;

		StartCoroutine("swoop_out");
	}

	public void goto_next_level () {
		if (current_state == game_grid.InGameState.SwoopOut)
			return;

		current_state = game_grid.InGameState.SwoopOut;

		StartCoroutine("swoop_out");
	}

	public void start_level () {

		set_cursor (-1, -1);
		
		current_state = game_grid.InGameState.SwoopIn;

		// start a coroutine that ends with setting current_state to Playing
		StartCoroutine("swoop_in");

	}

	public string text_a_string = "";
	public string text_b_string = "";

	public void load_level (int levelnum, int worldnum = 0) {

		num_joined_groups = 0;
		reset_join_tiles ();
		seq_lengths_done_this_level = false;

		text_a_string = "";
		text_b_string = "";

		// assets / levels / .json
		//Debug.Log("game_grid > load_level" + levelnum);

		//data_man d_script = GetComponent<data_man>();//.LoadLevelFile(levelnum);
		data_man.data_man_instance.LoadLevelFile(levelnum, worldnum);
		//d_script.LoadLevelFile(levelnum);

		if (data_man.data_man_instance.loaded_level.text_a != null &&
			data_man.data_man_instance.loaded_level.text_a != "")
			text_a_string = data_man.data_man_instance.loaded_level.text_a;

		if (data_man.data_man_instance.loaded_level.text_b != null &&
			data_man.data_man_instance.loaded_level.text_b != "")
			text_b_string = data_man.data_man_instance.loaded_level.text_b;


		for (int x = 0; x < grid_w; x++) {
			for (int y = 0; y < grid_h; y++) {
				block_script b_script = blocks [x, y].GetComponent<block_script>();
				b_script.reset ();
			}
		}


		for (int x = 0; x < grid_w; x++) {
			for (int y = 0; y < grid_h; y++) {
				var i = x + y*10;
				int tile = data_man.data_man_instance.loaded_level.floor[i];
				int cover = data_man.data_man_instance.loaded_level.cover[i];

				block_script b_script = blocks [x, y].GetComponent<block_script>();

				if (tile == 2) b_script.set_type(2);
				else if (tile == 1) b_script.set_type(1);
				else b_script.set_type(0);

				b_script.preset_hint(0);

				// clue
				if (tile == 5) b_script.preset_hint(block_script.HintType.FourTouch);	// 4 touch
				if (tile == 3) b_script.preset_hint(block_script.HintType.Eye);	// eye
				if (tile == 9) b_script.preset_hint(block_script.HintType.EightTouch);	
				if (tile == 10) b_script.preset_hint(block_script.HintType.Heart);	
				if (tile == 11) b_script.preset_hint(block_script.HintType.Compass);	
				if (tile == 12) b_script.preset_hint(block_script.HintType.Crown);	
				if (tile == 13) b_script.preset_hint(block_script.HintType.EyeBracket);	
				if (tile == 49) b_script.preset_hint(block_script.HintType.Zap);	
				if (tile == 80) b_script.preset_hint(block_script.HintType.TotalMines);	
				if (tile == 51) b_script.preset_hint(block_script.HintType.EyeRepeat);	

				if (tile == 16) {
					b_script.share_square = true;
					b_script.share_connect_left = true;
					b_script.share_connect_right = true;
					b_script.share_connect_down = true;
					b_script.share_connect_up = true;
				} else if (tile == 17) {
					b_script.share_pipe = true;
					b_script.share_connect_left = true;
					b_script.share_connect_right = true;
				} else if (tile == 18) {
					b_script.share_pipe = true;
					b_script.share_connect_down = true;
					b_script.share_connect_up = true;
				} else if (tile == 19) {
					// pipe corner L D
					b_script.share_pipe = true;
					b_script.share_connect_down = true;
					b_script.share_connect_left = true;
				} else if (tile == 20) {
					// pipe corner L U
					b_script.share_pipe = true;
					b_script.share_connect_up = true;
					b_script.share_connect_left = true;
				} else if (tile == 21) {
					// pipe corner R D
					b_script.share_pipe = true;
					b_script.share_connect_down = true;
					b_script.share_connect_right = true;
				} else if (tile == 22) {
					// pipe corner R U
					b_script.share_pipe = true;
					b_script.share_connect_up = true;
					b_script.share_connect_right = true;
				} else if (tile == 23) {
					// pipe corner L D R U
					b_script.share_pipe = true;
					b_script.share_connect_left = true;
					b_script.share_connect_right = true;
					b_script.share_connect_down = true;
					b_script.share_connect_up = true;
				} else if (tile == 29) {
					b_script.share_square = true;
					b_script.share_connect_right = true;
					b_script.share_connect_down = true;
				} else if (tile == 31) {
					b_script.share_square = true;
					b_script.share_connect_up = true;
					b_script.share_connect_down = true;
				} else if (tile == 26) {
					b_script.share_square = true;
					b_script.share_connect_left = true;
					b_script.share_connect_right = true;
				} else if (tile == 32) {
					b_script.share_square = true;
					b_script.share_connect_up = true;
					b_script.share_connect_right = true;
				} else if (tile == 27) {
					b_script.share_square = true;
					b_script.share_connect_up = true;
					b_script.share_connect_left = true;
				} else if (tile == 24) {
					b_script.share_square = true;
					b_script.share_connect_down = true;
					b_script.share_connect_left = true;
				} 


				if (tile == 8 && y > 0)
					join_tiles (x, y, x, y - 1);
				if (tile == 7 && x > 0)
					join_tiles (x, y, x - 1, y);

				// multi
				if (cover == 53) {
					b_script.set_multi (2);
				}
			}
		} // for

		// cover layer - triggers calculations
		for (int x = 0; x < grid_w; x++) {
			for (int y = 0; y < grid_h; y++) {
				var i = x + y*10;
				int cover = data_man.data_man_instance.loaded_level.cover[i];

				block_script b_script = blocks [x, y].GetComponent<block_script>();


				if (cover == 6 || cover == 53)
					b_script.cover ();
				else {
					b_script.cover ();
					b_script.uncover ();
				}

				if (b_script.block_type == 1) b_script.uncover ();
			}
		}

		calc_share_groups ();

		no_hearts_see_doubles ();
	}

	private void no_hearts_see_doubles() {
		Debug.Log ("todo: no_heaerts_see_doubles");

		for (int x = 0; x < grid_w; x++) {
			for (int y = 0; y < grid_h; y++) {
				if (block_scripts [x, y].mine_multi <= 1)
					continue;

				// mine multi 2
				// any hearts in this LOS ? gotta make them eyes
				// joined tiles are handled in uncover
				block_scripts [x, y].fill_range_line_of_sight ();

				for (int r = 0; r < block_scripts [x, y].x_in_range.Count; r++) {
					int xx = block_scripts [x, y].x_in_range [r];
					int yy = block_scripts [x, y].y_in_range [r];
					if (block_scripts [xx, yy].hint_type == block_script.HintType.Heart) {
						block_scripts [xx, yy].preset_hint (block_script.HintType.Eye);
						if (block_scripts [xx, yy].covered_up == false) {
							block_scripts [xx, yy].cover ();
							block_scripts [xx, yy].uncover ();
						}
					}
				}

				block_scripts [x, y].clear_range ();

			}
		}

	}

	public int get_multi(int x, int y) {
		if (x < 0 || y < 0 || x > grid_w - 1 || y > grid_h - 1)
			return 1;
		return block_scripts [x, y].mine_multi;
	}

	private int num_of_share_groups = 0;
	private int num_share_calc_recursions = 0;

	private void calc_share_groups() {
		num_of_share_groups = 0;
		num_share_calc_recursions = 0;



		// each block share_groups is reset to [] already on .reset()

		// first pass to setup sharesquares which are the 'flow' sources
		for (int x = 0; x < grid_w; x++) {
			for (int y = 0; y < grid_h; y++) {
				if (block_scripts [x, y].hint_type != 0)
					continue;
				if (block_scripts [x, y].share_square == false)
					continue;


				if (block_scripts [x, y].share_square == true &&
				    block_scripts [x, y].share_groups.Count == 0) {

					block_scripts [x, y].share_groups.Add (num_of_share_groups);
					num_of_share_groups++;

					// trim dangling sides (some maps have a UDLR share bubble with 2 connections)

				}
				
			}
		} // for x

		calc_share_groups_internal ();

		// include the terminal hints in the sharegroups
		for (int x = 0; x < grid_w; x++) {
			for (int y = 0; y < grid_h; y++) {
				if (block_scripts [x, y].hint_type == 0 &&
					block_scripts [x, y].join_group == 0)
					continue;	// note the AND

				block_scripts [x, y].share_groups = new List<int>();

				if (x > 0 &&
				    block_scripts [x - 1, y].share_groups.Count == 1 &&
				    block_scripts [x - 1, y].share_connect_right == true) {
					int s_group = block_scripts [x - 1, y].share_groups [0];
					block_scripts [x, y].link_hint_to_sharegroup (s_group);
				}

				if (x < grid_w - 1 &&
					block_scripts [x + 1, y].share_groups.Count == 1 &&
					block_scripts [x + 1, y].share_connect_left == true) {
					int s_group = block_scripts [x + 1, y].share_groups [0];
					block_scripts [x, y].link_hint_to_sharegroup (s_group);
				}

				if (y > 0 &&
					block_scripts [x, y - 1].share_groups.Count == 1 &&
					block_scripts [x, y - 1].share_connect_down == true) {
					int s_group = block_scripts [x, y - 1].share_groups [0];
					block_scripts [x, y].link_hint_to_sharegroup (s_group);
				}

				if (y < grid_h - 1 &&
					block_scripts [x, y + 1].share_groups.Count == 1 &&
					block_scripts [x, y + 1].share_connect_up == true) {
					int s_group = block_scripts [x, y + 1].share_groups [0];
					block_scripts [x, y].link_hint_to_sharegroup (s_group);
				}
			}
		} // for x

		// find uncovered sharesquares
		// cover / uncover

		for (int x = 0; x < grid_w; x++) {
			for (int y = 0; y < grid_h; y++) {
				if (block_scripts [x, y].share_square == false)
					continue;
				if (block_scripts [x, y].covered_up == true)
					continue;

				Debug.Log ("share_square starts uncovered x: " + x + " y: " + y + "share_groups: " + block_scripts[x,y].share_groups.Count);



				block_scripts [x, y].cover ();
				block_scripts [x, y].uncover ();
			}
		}

	}

	private void calc_share_groups_internal() {
		int loop_again = 1;
		num_share_calc_recursions++;
		if (num_share_calc_recursions > grid_w * grid_h)
			return;

		for (int x = 0; x < grid_w; x++) {
			for (int y = 0; y < grid_h; y++) {
				if (block_scripts [x, y].hint_type != 0)
					continue;

				// for each share group this tile is in (hints can be in multiple sharegroups)
				if (block_scripts[x,y].share_groups.Count == 0) continue;

				int s_group = block_scripts [x, y].share_groups [0];

				if (x > 0 &&
				    block_scripts [x, y].share_connect_left == true &&
				    block_scripts [x - 1, y].share_groups.Count == 0 &&
				    block_scripts [x - 1, y].block_type == 0 &&
				    block_scripts [x - 1, y].share_pipe == true &&
				    block_scripts [x - 1, y].share_connect_right == true) {
						block_scripts [x - 1, y].share_groups.Add (s_group);
						loop_again = 1;
				}

				if (x < grid_w - 1 &&
					block_scripts [x, y].share_connect_right == true &&
					block_scripts [x + 1, y].share_groups.Count == 0 &&
					block_scripts [x + 1, y].block_type == 0 &&
					block_scripts [x + 1, y].share_pipe == true &&
					block_scripts [x + 1, y].share_connect_left == true) {
						block_scripts [x + 1, y].share_groups.Add (s_group);
						loop_again = 1;
				}

				if (y > 0 &&
					block_scripts [x, y].share_connect_up == true &&
					block_scripts [x, y - 1].share_groups.Count == 0 &&
					block_scripts [x, y - 1].block_type == 0 &&
					block_scripts [x, y - 1].share_pipe == true &&
					block_scripts [x, y - 1].share_connect_down == true) {
						block_scripts [x, y - 1].share_groups.Add (s_group);
						loop_again = 1;
				}

				if (y < grid_h - 1 &&
					block_scripts [x, y].share_connect_down == true &&
					block_scripts [x, y + 1].share_groups.Count == 0 &&
					block_scripts [x, y + 1].block_type == 0 &&
					block_scripts [x, y + 1].share_pipe == true &&
					block_scripts [x, y + 1].share_connect_up == true) {
					block_scripts [x, y + 1].share_groups.Add (s_group);
					loop_again = 1;
				}

			}
		}

		if (loop_again == 1)
			calc_share_groups_internal ();

	}

	public bool is_flowfill_seen(int x, int y) {
		return block_scripts [x, y].flowfill_seen;
	}

	public void mark_flowfill_seen(int index) {
		int x = get_block_x_by_index (index);
		int y = get_block_y_by_index (index);
		block_scripts [x, y].flowfill_seen = true;
	}

	public void reset_flowfill() {
		for (int x = 0; x < grid_w; x++) {
			for (int y = 0; y < grid_h; y++) {
				block_scripts [x, y].flowfill_seen = false;
			}	
		}
	}

	public void generate_rand () {

		num_joined_groups = 0;
		reset_join_tiles ();
		seq_lengths_done_this_level = false;

		text_a_string = "";
		text_b_string = "";



		for (int x = 0; x < grid_w; x++) {
			for (int y = 0; y < grid_h; y++) {

				block_scripts [x, y].reset ();

				int rand_hint = UnityEngine.Random.Range(1,10);
				int rand_mine = UnityEngine.Random.Range(1,10);
				int rand_multi = UnityEngine.Random.Range(1,10);

				if (rand_multi < 2.5) {
					block_scripts [x, y].mine_multi = 2;
				}

				if (rand_mine < 4) {
					block_scripts [x, y].set_type (2);
					block_scripts [x, y].cover ();
					continue;
				}

				block_scripts [x, y].preset_hint(block_script.HintType.FourTouch);

				if (rand_hint <= 1) block_scripts [x, y].preset_hint(block_script.HintType.FourTouch);	// 4 touch
				else if (rand_hint <= 2) block_scripts [x, y].preset_hint(block_script.HintType.Eye);	// eye
				else if (rand_hint <= 3) block_scripts [x, y].preset_hint(block_script.HintType.EightTouch);	
				else if (rand_hint <= 4) block_scripts [x, y].preset_hint(block_script.HintType.Heart);	
				else if (rand_hint <= 5) block_scripts [x, y].preset_hint(block_script.HintType.Compass);	
				else if (rand_hint <= 6) block_scripts [x, y].preset_hint(block_script.HintType.Crown);	
				else if (rand_hint <= 7) block_scripts [x, y].preset_hint(block_script.HintType.EyeBracket);	
				else if (rand_hint <= 8) block_scripts [x, y].preset_hint(block_script.HintType.Zap);	
				else if (rand_hint <= 9) block_scripts [x, y].preset_hint(block_script.HintType.EyeRepeat);	

			}
		}

		for (int x = 0; x < grid_w; x++) {
			for (int y = 0; y < grid_h; y++) {
				int rand_cover = UnityEngine.Random.Range(1,10);

				block_scripts [x, y].cover ();

				if (rand_cover < 4 &&
				    get_block_type (x, y) != 2) {
					block_scripts [x, y].uncover ();
				}
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (screen_width != Screen.width ||
			screen_height != Screen.height) {

			screen_width = Screen.width;
			screen_height = Screen.height;
			on_resize ();
		}
	}

	public void on_resize () {
		

		float headspace = 0;
		float new_y = 0;
		float ratio = (float)Screen.height/(float)Screen.width; //screen_height / screen_width;	didnt work?!

		//Debug.Log ("ratio: " + ratio);

		// area_grid = 100*tile_size
		// area_all = 
		// if ratio == 1 -> area_all == area_grid
		// if ratio == 2 -> area_all == 2*area_grid
		// area_all = ratio*area_grid
		// vert_space = ratio*grid_height		not grid_h (10)
		// excess_vert_space = vert_space - grid_height = ratio*grid_height - grid_height
		// head_space = excess_vert_space*0.5

		if (ratio > 1) {
			// portrait
			float extra_v_space = ratio*tile_size*10 - tile_size*10;	// tile_size*10*(ratio - 1)
			headspace = extra_v_space * 0.5f;
			if (headspace > 0) new_y += headspace;
		}
		//Debug.Log ("headspace " + headspace);
		gameObject.transform.position = new Vector2 ( gameObject.transform.position.x, new_y );

		return;
		/*
		if (screen_width < screen_height) {
			Debug.Log ("screen_height " + screen_height);
			float headspace = screen_height*0.01f*0.5f - grid_h*0.5f*tile_size;

			if (headspace > 0) new_y += headspace;
		}*/

	}
}
