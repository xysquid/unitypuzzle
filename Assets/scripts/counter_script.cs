﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class counter_script : MonoBehaviour {

	public GameObject numeral_prefab;
	private GameObject[] numeral_object = new GameObject[3];

	private bool started = false; 

	// Use this for initialization
	void Awake () {
		started = true;
		for (int i = 0; i < 3; i++) {
			numeral_object[i] = Instantiate(numeral_prefab, new Vector2 (0, 0), Quaternion.identity) as GameObject;
			numeral_object[i].transform.SetParent (transform, false);
			numeral_object[i].GetComponent<numeral_script>().hide ();
		}
	}
	
	// Update is called once per frame
	//void Update () {
		
	//}

	public void hide () {
		for (int i = 0; i < 3; i++) {
			
			numeral_script n_script = numeral_object[i].GetComponent<numeral_script>();
			n_script.hide ();
		}

	}

	public void center_y () {

	}

	// need 3 char sprites for Mine of Sight

	public void set_num (int new_num) {
		// This function is being called before Start .. but not before Awake
		// Debug.Log ("counter_script > set_num ... this.started == " + started);

		string str = new_num.ToString ();
		int str_len = str.Length;



		int x_off = 0;

		if (new_num < 10) {
			//transform.numeral_one.hide ();



			numeral_object [0].transform.GetComponent<numeral_script>().set_x (0.0f);//.transform.localPosition.x = 0;
			numeral_script n_one_script = numeral_object[0].transform.GetComponent<numeral_script>();
			n_one_script.set_num (new_num);


		} else if (new_num >= 10 && new_num < 100) {
			x_off = -10;
			Debug.Log ("counter >= 10 !!!!");


			int tens = new_num / 10;
			int ones = new_num % 10;

			numeral_object [0].transform.GetComponent<numeral_script>().set_x (-0.1f);//.transform.localPosition.x = -10;
			numeral_script n_one_script = numeral_object[0].transform.GetComponent<numeral_script>();
			n_one_script.set_num (tens);

			numeral_object [1].transform.GetComponent<numeral_script>().set_x (0.1f);//.transform.localPosition.x = 10;
			numeral_script n_two_script = numeral_object[1].transform.GetComponent<numeral_script>();
			n_two_script.set_num (ones);

		}

		//for (var i = 0; i < char_sprites.length; i++) {

		//}



		//transform.numeral_one.set_num (new_num);


	}
}
