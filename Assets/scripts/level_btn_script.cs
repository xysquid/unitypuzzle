﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class level_btn_script : MonoBehaviour {

	public int levelnum;
	public Component textobj;
	public level_grid theparent;
	public Sprite done_level_sprite;
	public Sprite level_sprite;

	public GameObject level_icon;	// child

	// Use this for initialization
	void Start () {
		//textobj = gameObject.GetComponent<UnityEngine.UI.Text> ();

		theparent = GetComponentInParent<level_grid> ();


	}

	public void set_done_or_not (bool done_) {
		if (done_ == false)
			GetComponent<SpriteRenderer> ().sprite = level_sprite;
		else GetComponent<SpriteRenderer> ().sprite = done_level_sprite;
	}

	public void show_text () {
		GetComponentInChildren<TextMesh> ().text = (levelnum + 1).ToString ();
	
		//GetComponent<Text> ().text = levelnum.ToString();

	}

	public void hide () {
		gameObject.SetActive (false);
	}

	public void make_vis () {
		gameObject.SetActive (true);
	}

	public void add_icon () {
		if (levelnum == 0) {
			level_icon.GetComponent<SpriteRenderer> ().enabled = true;
			level_icon.GetComponent<SpriteRenderer> ().sprite = theparent.hand_sprite;
		} else if (levelnum == 2) {
			level_icon.GetComponent<SpriteRenderer> ().enabled = true;
			level_icon.GetComponent<SpriteRenderer> ().sprite = theparent.eye_sprite;
		} else if (levelnum == 13) {
			level_icon.GetComponent<SpriteRenderer> ().enabled = true;
			level_icon.GetComponent<SpriteRenderer> ().sprite = theparent.eight_sprite;
		} else if (levelnum == 28) {
			level_icon.GetComponent<SpriteRenderer> ().enabled = true;
			level_icon.GetComponent<SpriteRenderer> ().sprite = theparent.jointut_sprite;
		} else if (levelnum == 53) {
			level_icon.GetComponent<SpriteRenderer> ().enabled = true;
			level_icon.GetComponent<SpriteRenderer> ().sprite = theparent.heart_sprite;
		} else if (levelnum == 80) {
			level_icon.GetComponent<SpriteRenderer> ().enabled = true;
			level_icon.GetComponent<SpriteRenderer> ().sprite = theparent.compass_sprite;
		} else if (levelnum == 90) {
			level_icon.GetComponent<SpriteRenderer> ().enabled = true;
			level_icon.GetComponent<SpriteRenderer> ().sprite = theparent.eyebracket_sprite;
		} else if (levelnum == 100) {
			level_icon.GetComponent<SpriteRenderer> ().enabled = true;
			level_icon.GetComponent<SpriteRenderer> ().sprite = theparent.crown_sprite;
		} else if (levelnum == 110) {
			level_icon.GetComponent<SpriteRenderer> ().enabled = true;
			level_icon.GetComponent<SpriteRenderer> ().sprite = theparent.sharetut_sprite;
		} else if (levelnum == 130) {
			level_icon.GetComponent<SpriteRenderer> ().enabled = true;
			level_icon.GetComponent<SpriteRenderer> ().sprite = theparent.zap_sprite;
		} else if (levelnum == 160) {
			level_icon.GetComponent<SpriteRenderer> ().enabled = true;
			level_icon.GetComponent<SpriteRenderer> ().sprite = theparent.eyerepeat_sprite;
		} else {
			level_icon.GetComponent<SpriteRenderer> ().enabled = false;
		}


		return;
		/*
		if (theparent.level_icons.ContainsKey (levelnum) == false) {
			level_icon.GetComponent<SpriteRenderer>().enabled = false;
			return;
		}

		Sprite spriteref;

		bool hasValue = theparent.level_icons.TryGetValue (levelnum, spriteref);

		if (hasValue == false)
			return;

		level_icon.GetComponent<SpriteRenderer>().enabled = true;
		level_icon.GetComponent<SpriteRenderer>().sprite = spriteref;

		Debug.Log ("level button icon! " + levelnum);
		*/
	}

	void OnMouseDown () {
		Debug.Log ("button for level " + levelnum + "clicked");
		theparent.goto_level (levelnum);
	}

	// Update is called once per frame
	void Update () {
		
	}
}
