﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine;

public class action_buttons_script : MonoBehaviour, IPointerUpHandler, IPointerDownHandler {

	public int button_selected = 0;	// 0 nothing 1 dig 2 flag

	public GameObject flag_button;
	public GameObject dig_button;
	public GameObject select_button;


	// Use this for initialization
	void Start () {
		select_button.SetActive (false);	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void hide() {
		gameObject.SetActive (false);
	}

	public void make_vis() {
		gameObject.SetActive (true);
	}

	public void on_press_dig () {
		// 

		// highlight dig button


		// tell game_grid that dig is selected (in case user taps a tile)
		// or in case we are in gamepad mode, in which case a dig is triggered
		game_grid.game_grid_instance.select_dig();
	}

	public void on_press_flag () {
		// 

		// highlight dig button

		// tell game_grid that dig is selected (in case user taps a tile)
		// or in case we are in gamepad mode, in which case a dig is triggered
		game_grid.game_grid_instance.select_flag();
	}


	// these should probably go in the child buttons
	public virtual void OnPointerDown (PointerEventData ped) {
		//Vector2 gamepad_pos = ped.position - transform.position;
	}

	public virtual void OnPointerUp (PointerEventData ped) {
		
	}

	public void on_resize () {
		if (Screen.width > Screen.height) {
			flag_button.transform.position = new Vector2 ((float)Screen.width - 26f, (float)Screen.height * 0.33f);
			dig_button.transform.position = new Vector2((float)Screen.width - 26f,  (float)Screen.height * 0.66f);
		} else {
			flag_button.transform.position = new Vector2 ((float)Screen.width*0.33f, 26f);
			dig_button.transform.position = new Vector2((float)Screen.width*0.66f,  26f);

		}
	}
}
