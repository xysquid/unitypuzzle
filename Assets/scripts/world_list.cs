﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class world_list : MonoBehaviour {

	public GameObject world_prefab;

	private GameObject[] worlds = new GameObject[6];
	private GameObject[] worldnames = new GameObject[6];
	private GameObject[] worlddescs = new GameObject[6];

	static int starting_world = 0;
	static int max_worlds = 0;

	private bool touchable_buttons = true;

	// Use this for initialization
	void Start () {
		for (int w = 0; w < 6; w++) {
			// relative position to parent world_list (this)
			worlds[w] = Instantiate (world_prefab, new Vector2 (-1.5f, 3f - w * 1f), Quaternion.identity) as GameObject;
			worlds[w].transform.SetParent (transform, false);

			worldnames [w] = worlds [w].transform.Find ("WorldName").gameObject;
			worlddescs [w] = worlds [w].transform.Find ("WorldDesc").gameObject;

			world_button_script w_script = worlds [w].GetComponent<world_button_script>();
			w_script.buttonnum = w;
		}

		show_worlds ();
	}

	public void on_button_click (int button) {
		int w = button - starting_world;

		scene_manager.scene_manager_instance.goto_overworld (w);
	}

	public void show_worlds () {
		max_worlds = data_man.data_man_instance.all_worlds.Count;

		// Want to put id 0 (the campaign levels) first
		//data_man.data_man_instance.all_worlds.Sort ();

		for (int i = 0; i < 6; i++) {
			worlds [i].SetActive (false);
		}

		for (int w = starting_world; w < starting_world + 6; w++) {
			if (w > max_worlds)
				break;
			int i = w - starting_world;
			worlds [i].SetActive (true);

			worldnames [w].GetComponent<TextMesh> ().text = data_man.data_man_instance.get_world_name (i);
			worlddescs [w].GetComponent<TextMesh> ().text = data_man.data_man_instance.get_world_desc (i);

		}
	}

	public void next_page() {
		if (world_list.starting_world > max_worlds - 30)
			return;

		world_list.starting_world += 30;

		//touchable_buttons = false;	// ignore any input while doing the swoop out effect

		//StartCoroutine("page_right");

		scene_manager.scene_manager_instance.goto_world_list ();

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
