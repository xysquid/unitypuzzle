﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class menu_script : MonoBehaviour {

	// to stop pop_down clicks being counted as in-game dig/flag
	// maybe menu_up needs 3 states (full up, full down, inbetween) ... 4 states?
	// so game_grid checks this object (this object can be global)
	// OR - this object receives onmousedown and relays the click to game_grid, or not

	public bool menu_up = false;
	int target_x = 0;//-200;

	public static int g_current_level = 0;

	private int width = 199;

	void Awake () {
		// persist across all scence
		DontDestroyOnLoad (transform.gameObject);
	}

	// Use this for initialization
	void Start () {
		//transform.position.x = -200;


	}

	public void set_width(int new_w) {
		width = new_w;
	}


	IEnumerator do_pop_up () {

		float resting_x = (float)target_x; //190f
		float distance_x = (float)transform.position.x - (float)target_x ;	// 
		float swoop_time = 0.2f;	// 0.2 seconds

		float current_x = resting_x + distance_x; // (float)transform.position.x
		float start_time = Time.time; // seconds since 1970

		for (float time_passed = 0f; time_passed < swoop_time; time_passed = Time.time - start_time) {
			
			current_x = distance_x + resting_x - (time_passed / swoop_time)  * distance_x;

			transform.position = new Vector3 ((float)current_x, transform.position.y, transform.position.z);

			yield return null;

		}

		transform.position = new Vector3 (resting_x, transform.position.y, transform.position.z);
		Debug.Log ("side menu x now = " + resting_x);

	}

	IEnumerator do_pop_down () {
		yield return null;
	}

	// Update is called once per frame
	void Update () {
		//if (menu_up == false && transform.position.x > target_x)
		//	transform.Translate (Vector2.left * 10);
		//else if (menu_up == true && transform.position.x < target_x) {
		//	transform.Translate (Vector2.right * 10);
		//}

		if (menu_up == true && Input.GetMouseButtonDown (0)) {
			Debug.Log ("menu hears a mousedownnnnnnnnnn");
		}

		if (menu_up == true && Input.GetMouseButtonDown (0) && Input.mousePosition.x > width) {
			pop_down ();
		}
	}

	public void goto_random() {
		scene_manager.scene_manager_instance.goto_random ();
	}

	public void goto_levels() {
		//Application.LoadLevel ("Levels");

		//scene_manager.scene_manager_instance.goto_overworld ();

		scene_manager.scene_manager_instance.goto_world_list ();
	}

	public void OnMouseDown() {
		Debug.Log ("menu hears a mousedown");
	}

	public void pop_up () {
		target_x = width - 5;//190;//90;
		menu_up = true;

		StartCoroutine("do_pop_up");

		//Debug.Log ("menu rect width " + GetComponent<>);
	}

	public void pop_down () {
		target_x = 0;//-200;
		menu_up = false;
		StartCoroutine("do_pop_up");
	}

	public void on_resize() {

	}
}
