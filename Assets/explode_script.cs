﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class explode_script : MonoBehaviour {

	public Sprite pop_a;
	public Sprite pop_b;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void start_pop(int x, int y) {
		gameObject.SetActive (true);
		GetComponent<SpriteRenderer> ().sprite = pop_a;
		transform.position = new Vector2 ((float)x, (float)y);
		StartCoroutine("do_pop");
	}

	IEnumerator do_pop() {

		for (int t = 0; t < 100; t += Random.Range(3,10)) {
			yield return null;
		}

		GetComponent<SpriteRenderer> ().sprite = pop_b;

		for (int t = 0; t < 100; t += Random.Range(3,10)) {
			yield return null;
		}

		gameObject.SetActive (false);

	}
}
